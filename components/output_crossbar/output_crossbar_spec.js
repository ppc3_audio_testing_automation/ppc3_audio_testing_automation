/**
 * This file has the test specs for the inputMixer component
 */

var testCasesJson = require('./output_crossbar_testcases.json'); //Retrieving the testcases of inputMixer from a JSON file 
var oc_utils = require('./output_crossbar_Utils.js');

var common_utils = require('../commonUtils.js');
var audio_player = require('../audio_player/audio_player_operations.js');
var path = require('path');
var configuration;

module.exports = {
    test: function(rootPath, runInput, audioPlayer, config_name, audioModeElement) {
        var defer = protractor.promise.defer();
        /**
         * Getting the testcases from the JSON file based on the deviceType (Mono or Stereo)
         */
        // Iterating through the testcases 
        var testCases;
        /**
         * This function will split the test cases for mono configuration and stereo configuration
         */
        testCases = common_utils.findTestCasesToRun(testCasesJson.testCases, runInput, "output_crossbar");
        testCases.forEach(function(testCaseObject, posit) {
            /**
             * This spec is to apply the testcases
             * applyInput() - applies the input values
             * checkAdvancedPage() - checks the advanced page whether the changes are replicated or not
             * recordAudio() - uploads the audio of the particular testcase and records it
             * dumpAudioFile() - dumps the recorded audio(.json) from the source to the reference_audio
             */
            it(testCaseObject.id, function() {
                browser.ignoreSynchronization = true;
                oc_utils.applyInput(testCaseObject).then(function() {
                    oc_utils.checkAdvancedPage(testCaseObject).then(function() {
                        common_utils.recordAudio(rootPath, audio_player, testCaseObject, testCasesJson.defaultAudioInput).then(function() {
                            common_utils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCaseObject.id, config_name, audioModeElement, 'A', runInput.appToTest);
                            common_utils.applySnapshot();
                        });
                    });
                }, function() {
                    console.log('testcase not applicable!!!');
                }).then(function() {
                    common_utils.isDeviceBAvailable().then(function() {
                        common_utils.switchDevice('B');
                        oc_utils.applyInput(testCaseObject).then(function() {
                            oc_utils.checkAdvancedPage(testCaseObject).then(function() {
                                common_utils.recordAudio(rootPath, audio_player, testCaseObject, testCasesJson.defaultAudioInput).then(function() {
                                    common_utils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCaseObject.id, config_name, audioModeElement, 'B', runInput.appToTest);
                                    common_utils.applySnapshot();
                                });
                            });
                        });
                        common_utils.switchDevice('A');
                    }, function() {});
                });
            });
        });
        return defer.promise;
    }
};
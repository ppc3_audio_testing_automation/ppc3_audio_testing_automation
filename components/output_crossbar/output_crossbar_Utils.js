var shortDisplay = require('../abbreviations.json');
var common_utils = require('../commonUtils.js');
var input_mixer_utils = require('../input_mixer/input_mixer_Utils.js'); //Importing the functions of inputMixer
var fs = require('fs');
var loopAsync = require('../lib/loopAsync.js');
var chai = require('chai');
var expect = chai.expect;
var dom_ocrosbar;
module.exports = {
    /**
     * This function will is used to apply input for the channels
     */
    "applyInput": function(testCaseObject) {
        var defer = protractor.promise.defer();
        var channelInputs = [];
        common_utils.switchOnComponent('Output Crossbar').then(function(componentIndex) {
            dom_ocrosbar = element.all(by.css('.grid_innerdiv')).get(componentIndex);
        }).then(function() {
            Object.keys(testCaseObject.basic).forEach(function(inputs) {
                channelInputs.push(testCaseObject.basic[inputs]);
            });
            input_mixer_utils.applyChannelInput(channelInputs, dom_ocrosbar).then(function() {
                console.log('applyChannelInpt passedu');
                defer.fulfill();
            }, function() {
                console.log('rejected');
                defer.reject();
            });
        });
        return defer.promise;
    },
    /**
     * This funtion is used to check the advanced page values
     */
    "checkAdvancedPage": function(testCaseObject) {
        var defer = protractor.promise.defer();
        input_mixer_utils.changeMode('advanced', dom_ocrosbar, 'ocross-bas-adv-btn-holder').then(function() {
            var left_row = dom_ocrosbar.all(by.repeater('dispIn in crossbarData.displayInput')).get(0);
            var right_row = dom_ocrosbar.all(by.repeater('dispIn in crossbarData.displayInput')).get(1);
            var keys = Object.keys(testCaseObject.advanced);
            var index = 0;
            dom_ocrosbar.all(by.repeater('dispIn in crossbarData.displayInput')).each(function(row_dom, row_count) {
                row_dom.all(by.repeater('advPropName in crossbarData.advProps[dispIn].propNames')).each(function(box) {
                    box.all(by.model('crossbarData.props[advPropName].value')).first().getAttribute('value').then(function(value) {
                        console.log('actual : ', testCaseObject.advanced[keys[index]], 'real : ', value);
                        expect(testCaseObject.advanced[keys[index]].toString()).to.equal(value);
                        index++;
                    });
                });
            });
        });
        input_mixer_utils.changeMode('basic', dom_ocrosbar, 'ocross-bas-adv-btn-holder', 'custom-popup-btns').then(function() {
            defer.fulfill();
        });
        return defer.promise;
    }
};
var volume = require('./volumeUtils.js');
var commonUtils = require('../commonUtils.js');
var path = require('path');
var chai = require('chai');
var expect = chai.expect;

module.exports = {
    /**
     * Tests the volume.
     * @param rootPath Current working directory path.
     * @param runInput Contents of config.json file.
     * @param audioPlayer audio player component to call its functions.
     * @param currentConfig Name of the configurations which is currently opened.
     * @param currentAudioMode Name of the audio mode which is currently opened.
     */
    test: function(rootPath, runInput, audioPlayer, currentConfig, currentAudioMode) {
        var defer = protractor.promise.defer();
        testInputs = require('./volume_testcases.json');
        testCases = commonUtils.findTestCasesToRun(testInputs.testCases, runInput, "volume");
        testCases.forEach(function(testCase, testCaseIndex) {
            it("should write test case : " + testCase.id, function() {
                volume.setVolume(testCase.left, testCase.right)
                commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                    commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'A', runInput.appToTest);
                })
                commonUtils.applySnapshot();
                commonUtils.isDeviceBAvailable().then(function() {
                    commonUtils.switchDevice('B');
                    volume.setVolume(testCase.left, testCase.right);
                    commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                        commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'B', runInput.appToTest);
                    })
                    commonUtils.applySnapshot();
                    commonUtils.switchDevice('A');
                }, function(error) {});
                if (testCaseIndex === testCases.length - 1) {
                    defer.fulfill();
                }
            })
        })
        return defer.promise;
    }
}
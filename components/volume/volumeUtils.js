var fs = require('fs');
var chai = require('chai');
var expect = chai.expect;
module.exports = {
    /**
     * sets the volume.
     * @param left volume to get applied to left channel.
     * @param right volume to get applied to right channel.
     */
    "setVolume": function(left, right) {
        browser.wait(function() {
            return element.all(by.css(".digi-gain-ddown-speaker")).first().isDisplayed();
        }, 10000, "waiting for volume icon");
        element.all(by.css(".digi-gain-ddown-speaker")).first().click();
        browser.wait(function() {
            return element.all(by.css(".digigain-vertical-slider-value")).first().isDisplayed();
        }, 10000, "waiting for volume slider field");
        browser.sleep(150);
        if (left !== undefined) {
            element.all(by.css(".digigain-vertical-slider-value")).first().click();
            browser.wait(function() {
                return element.all(by.css(".digigain-vertical-slider-txtbox")).first().isDisplayed();
            }, 10000, "waiting for volume slider textbox");
            element.all(by.css(".digigain-vertical-slider-txtbox")).first().clear();
            element.all(by.css(".digigain-vertical-slider-txtbox")).first().sendKeys(left);
        }
        if (right !== undefined) {
            var checkBox = element.all(by.css(".checkboxcss3.digi-gain")).first().all(by.tagName("label")).first();
            checkBox.isDisplayed().then(function(displayed) {
                if (displayed) {
                    checkBox.click();
                    browser.wait(function() {
                        return element.all(by.css(".digigain-vertical-slider-value")).get(1).isDisplayed();
                    }, 10000, "waiting for volume slider field of right channel");
                    element.all(by.css(".digigain-vertical-slider-value")).get(1).click();
                    browser.wait(function() {
                        return element.all(by.css(".digigain-vertical-slider-txtbox")).get(1).isDisplayed();
                    }, 10000, "waiting for volume slider text box of right channel");
                    element.all(by.css(".digigain-vertical-slider-txtbox")).get(1).clear();
                    element.all(by.css(".digigain-vertical-slider-txtbox")).get(1).sendKeys(right);
                }
            })
        }
    }
}
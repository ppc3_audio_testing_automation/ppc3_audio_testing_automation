var fs = require('fs');
var path = require('path');
var commonUtils = require('../commonUtils.js');
var waitAbsent = require('../lib/waitAbsent.js');
var chai = require('chai');
var expect = chai.expect;
module.exports = {
    /**
     * sets the mixer gain.
     * @param bandContainer container for the specified band.
     * @param value value to be set as mixerGain in the corresponding band
     */
    setMixerGain: function(bandContainer, value) {
        var defer = protractor.promise.defer();
        bandContainer.all(by.model('band.props.mixerGain.value')).first().getAttribute('class').then(function(classValue) {
            if (classValue.indexOf('disableit') < 0) {
                bandContainer.all(by.model('band.props.mixerGain.value')).first().isDisplayed().then(function(displayed) {
                    if (displayed) {
                        bandContainer.all(by.model('band.props.mixerGain.value')).first().clear();
                        bandContainer.all(by.model('band.props.mixerGain.value')).first().sendKeys(value);
                        defer.fulfill();
                    } else {
                        console.log("Skipped : mixer gain not set to " + value + ' as mixergain input is not present');
                    }
                })
            }
        })
    },
    /**
     * set the values for properties in region.
     * @param bandContainer container for the specified band.
     * @param key name of the property to be set.
     * @param region test cases to be set for the region. 
     */
    setRegionProperty: function(bandContainer, key, region) {
        bandContainer.all(by.css('.nw-drc-rb-tile-table-other-title')).reduce(function(result, titleElement, index) {
            if (result > -1) { return result; }
            return titleElement.getText().then(function(title) {
                if (title.toLowerCase() === key.toLowerCase()) {
                    return index;
                }
            })
        }).then(function(keyIndex) {
            if (keyIndex !== undefined) {
                if (region.id === undefined) {
                    region.id = 0;
                }
                bandContainer.all(by.repeater('region in band.drcRegions.regions')).count().then(function(count) {
                    if (region.id < count) {
                        var regionRow = bandContainer.all(by.repeater('region in band.drcRegions.regions')).get(region.id);
                        regionRow.all(by.css('.nw-drc-gdata-input-ctrl.width-100percent')).get(keyIndex).getAttribute('class').then(function(value) {
                            if (value.indexOf('disableit') < 0) {
                                regionRow.all(by.css('.nw-drc-gdata-input-ctrl.width-100percent')).get(keyIndex).clear();
                                regionRow.all(by.css('.nw-drc-gdata-input-ctrl.width-100percent')).get(keyIndex).sendKeys(region[key]);
                            } else {
                                console.log('Skipped : Value not set for ' + key + ' as ' + region[key] + ' since input is disabled');
                            }
                        })
                    } else {
                        console.log('Skipped : region' + region.id + ' not present. So its properties are not set');
                    }
                });
            } else {
                console.log('Skipped : ', key + ' since it is not present');
            }
        })
    },
    /**
     * Turns on DRC component.
     */
    turnOnDrc: function() {
        browser.wait(function() {
            return element(by.id('main-content-app')).isDisplayed();
        }, 10000, 'Waiting for drc switch to appear');
        element(by.css('[title="Switch On DRC"]')).isDisplayed().then(function(displayed) {
            if (displayed) {
                element(by.css('[title="Switch On DRC"]')).click();
            }
        })
    },
    /**
     * Checks whether the given key is present in the given object.
     * @param obj object to check for the presence of given key.
     * @param key key to check for its presence in the object.
     */
    isKeyPresent: function(obj, key) {
        if (Object.keys(obj).indexOf(key) > -1) {
            return true;
        }
        return false;
    },
    /**
     * Applies the test case.
     * @param testCase test case object.
     */
    applyTestCase: function(testCase) {
        var self = module.exports;
        if (self.isKeyPresent(testCase, 'bands')) {
            self.setBandProperty(testCase).then(function() {});
        }
        if (self.isKeyPresent(testCase, 'crossOver')) {
            self.changeCrossOverProperty(testCase).then(function() {});
        }
    },
    /**
     * sets the properties of time constants.
     * @param bandContainer container of the specific band.
     * @param band testcase for the specific band.
     */
    setTimeConstantProperty: function(bandContainer, band) {
        Object.keys(band.timeConstants).forEach(function(key) {
            bandContainer.all(by.css('.nw-drc-gdata-controls-container.mgn-top-10px')).first().all(by.css('.nw-drc-rb-tile-table-other-title')).reduce(function(result, titleElement, index) {
                if (result > -1) {
                    return result;
                }
                return titleElement.getText().then(function(title) {
                    if (title.toLowerCase() === key.toLowerCase()) {
                        return index;
                    }
                })
            }).then(function(constantIndex) {
                if (constantIndex !== undefined) {
                    var timeConstantRow = bandContainer.all(by.css('.nw-drc-rb-tile-table-row.mgn-btm-zero')).first();
                    timeConstantRow.all(by.css('.nw-drc-gdata-input-ctrl.width-100percent')).get(constantIndex).getAttribute('class').then(function(value) {
                        if (value.indexOf('disableit') < 0) {
                            timeConstantRow.all(by.css('.nw-drc-gdata-input-ctrl.width-100percent')).get(constantIndex).clear();
                            timeConstantRow.all(by.css('.nw-drc-gdata-input-ctrl.width-100percent')).get(constantIndex).sendKeys(band.timeConstants[key]);
                        } else {
                            console.log('Skipped : value not set for ' + key + ' as ' + band.timeConstants[key] + '. The input is disabled.')
                        }
                    })
                } else {
                    console.log('Skipped : ' + key + ' not present. So its value is not set to ' + band.timeConstants[key]);
                }
            })
        })
    },
    /**
     * selects the crossover mode.
     * @param mode name of mode (basic or adv)
     */
    selectCrossOverMode: function(mode) {
        mode = mode.toLowerCase();
        element(by.css('.popup-basic-ap')).isDisplayed().then(function(displayed) {
            if (displayed) {
                element(by.css('.accept-restore-default')).click();
            }
        })
        browser.wait(function() {
            return element(by.id('drc-' + mode + '-holder')).isDisplayed();
        }, 10000, 'Waiting for ' + mode + ' crossover holder to be displayed');
        element(by.id('drc-' + mode + '-holder')).getAttribute('class').then(function(classValue) {
            if (classValue.indexOf('active') < 0) {
                element(by.id('drc-' + mode + '-holder')).click();
            }
        })
    },
    /**
     * sets the cross over band to low or medium or high.
     * @param bandType type of band(low,medium or high)
     */
    setCrossOverBand: function(bandType) {
        var defer = protractor.promise.defer();
        element.all(by.css('.nw-drc-right-top-container')).first().all(by.css('.crossover-tab-head')).reduce(function(bandIndex, bandElement, index) {
            if (bandIndex > -1) {
                return bandIndex;
            }
            return bandElement.isDisplayed().then(function(displayed) {
                if (displayed) {
                    return bandElement.getText().then(function(bandName) {
                        if (bandName.toLowerCase() === bandType.toLowerCase()) {
                            return index;
                        }
                    })

                }
            })
        }).then(function(bandIndex) {
            if (bandIndex !== undefined) {
                element.all(by.css('.nw-drc-right-top-container')).first().all(by.css('.crossover-tab-head')).get(bandIndex).getAttribute('class').then(function(classValue) {
                    if (classValue.indexOf('active') < 0) {
                        element.all(by.css('.nw-drc-right-top-container')).first().all(by.css('.crossover-tab-head')).get(bandIndex).click();
                    }
                    defer.fulfill();
                })
            } else {
                defer.reject('failed : ' + bandType + ' is not present');
            }
        })
        return defer.promise;
    },
    /**
     * sets the cross over filter.
     * @param filterHolder the container which is displayed.
     * @param filterName the type of filter. e.g. low f1
     */
    setCrossOverFilter: function(filterHolder, filterName) {
        var defer = protractor.promise.defer();
        browser.waitForAngular();
        var filterIndex;
        filterHolder.all(by.repeater('(key,bq) in drcData.drc.bands[$index].drcBqs.crossoverInstances')).reduce(function(filterIndex, subBandElement, index) {
            if (filterIndex > -1) {
                return filterIndex;
            }
            return subBandElement.getText().then(function(subBandName) {
                if (subBandName.toLowerCase() === filterName.toLowerCase()) {
                    return index;
                }
            })
        }).then(function(filterIndex) {
            if (filterIndex !== undefined) {
                filterHolder.all(by.repeater('(key,bq) in drcData.drc.bands[$index].drcBqs.crossoverInstances')).get(filterIndex).getAttribute('class').then(function(classValue) {
                    if (classValue.indexOf('active') < 0) {
                        filterHolder.all(by.repeater('(key,bq) in drcData.drc.bands[$index].drcBqs.crossoverInstances')).get(filterIndex).click();
                    }
                    defer.fulfill();
                })
            } else {
                defer.reject('failed : filter is not present');
            }
        });
        return defer.promise;
    },
    /**
     * gets the container which is displayed.
     */
    getFilterHolder: function() {
        var defer = protractor.promise.defer();
        element(by.css('.crossover-tab-body-container')).isDisplayed().then(function(displayed) {
            if (displayed) {
                defer.fulfill(element(by.css('.crossover-tab-body-container')));
            }
        });
        return defer.promise;
    },
    /**
     * sets the filter type.
     * @param filterHolder the container of the properties which is displayed.
     * @param filterType name of the filter type.
     */
    setFilterType: function(filterHolder, filterType) {
        var defer = protractor.promise.defer();
        var dropdownIndex;
        if (filterType === undefined) {
            defer.fulfill();
        } else {
            var isSet = false;
            filterHolder.all(by.css('select[name="filter-type"]')).reduce(function(isFilterTypeSet, dropdown, index) {
                if (isFilterTypeSet) {
                    return;
                }
                return dropdown.isDisplayed().then(function(displayed) {
                    if (displayed) {
                        dropdownIndex = index;
                        dropdown.all(by.tagName('option')).reduce(function(isOptionSelected, option) {
                            if (isOptionSelected) {
                                return;
                            }
                            return option.getText().then(function(optionVal) {
                                if (optionVal.toLowerCase() === filterType.toLowerCase()) {
                                    option.click();
                                    isSet = true;
                                    return true;
                                }
                            })
                        })
                        return true;
                    }
                })
            }).then(function() {
                if (!isSet) {
                    console.log('Skipped: Filter type not set as ' + filterType + ',since it is not present');
                }
                defer.fulfill();
            })
        }
        return defer.promise;
    },
    /**
     * sets the property in crossover.
     * @param filterHolder the container of the properties which is displayed.
     * @param property the name of the property for which value has to be set.
     * @param filterValue the value for the corresponding property.
     */
    setCrossOverProperty: function(filterHolder, property, filterValue) {
        var isSet = false;
        filterHolder.all(by.css('.dpq-panel-other-controls-holder')).reduce(function(result, inputHolder, index) {
            if (result) {
                return result;
            }
            return inputHolder.isDisplayed().then(function(displayed) {
                if (displayed) {
                    return inputHolder.all(by.css('.field-heading')).first().getText().then(function(heading) {
                        if (heading.toLowerCase() === property.toLowerCase()) {
                            return inputHolder.all(by.css('.nw-gdata-input-ctrl')).first().getAttribute('class').then(function(value) {
                                isSet = true;
                                if (value.indexOf('disableit') < 0) {
                                    inputHolder.all(by.css('.nw-gdata-input-ctrl')).first().clear();
                                    inputHolder.all(by.css('.nw-gdata-input-ctrl')).first().sendKeys(filterValue);
                                } else {
                                    console.log('Skipped : ' + property + ' is not set to ' + filterValue + ' as the input is disabled');
                                }
                                return true;
                            })
                        }
                    })
                }
            })
        }).then(function() {
            if (!isSet) {
                console.log('Skipped : ' + property + ' not set to ' + filterValue + ' as the property is not present.');
            }
        })
    },
    /**
     * updates the properties in the I/O bands
     * @param testCase testcase for the band.
     */
    setBandProperty: function(testCase) {
        var self = module.exports;
        var defer = protractor.promise.defer();
        testCase.bands.forEach(function(band) {
            browser.wait(function() {
                return element.all(by.css('.nw-drc-tile-heading')).first().isDisplayed();
            }, 10000, "Waiting for bands to appear");
            if (Object.keys(band).indexOf('name') < 0) {
                band.name = "Low";
            }
            var isSet = false;
            element.all(by.css('.nw-drc-tt-graph-holder')).reduce(function(result, bandContainer) {
                if (result) {
                    return result;
                }
                return bandContainer.all(by.css('.nw-drc-tile-heading')).first().getText().then(function(bandName) {
                    if (bandName.toLowerCase().indexOf(band.name.toLowerCase()) > -1) {
                        isSet = true;
                        if ((band.mute !== "true") && (band.switch !== 'off')) {
                            if (Object.keys(band).indexOf('mixerGain') > -1) {
                                self.setMixerGain(bandContainer, band.mixerGain);
                            }
                            if (Object.keys(band).indexOf('regions') > -1) {
                                band.regions.forEach(function(region) {
                                    Object.keys(region).forEach(function(key) {
                                        if (key !== 'id') {
                                            self.setRegionProperty(bandContainer, key, region);
                                        }
                                    })
                                })
                            }
                            if (Object.keys(band).indexOf('timeConstants') > -1) {
                                self.setTimeConstantProperty(bandContainer, band);
                            }
                        } else if (band.switch === 'off') {
                            bandContainer.all(by.css('.on-switch')).first().click();
                        } else if (band.mute === 'true') {
                            bandContainer.all(by.css('.nw-drc-bands-solomute-holder')).reduce(function(result, option, index) {
                                if (result > -1) {
                                    return result;
                                }
                                return option.getText().then(function(value) {
                                    if (value === 'Mute') {
                                        return index;
                                    }
                                })
                            }).then(function(optionIndex) {
                                bandContainer.all(by.css('.nw-drc-bands-solomute-holder')).get(optionIndex).click();
                            })
                        }
                        return true;
                    }
                })
            }).then(function() {
                if (!isSet) {
                    console.log('Skipped : ' + band.name + ' is not present so its properties are not set.');
                }
                defer.fulfill();
            })
        })
        return defer.promise;
    },
    /**
     * sets the filter in the crossover.
     * @param filterHolder the container of the properties which is displayed.
     * @param filterName name of the filter e.g. low f1
     */
    setFilter: function(filterHolder, filterName) {
        var self = module.exports;
        var defer = protractor.promise.defer();
        browser.waitForAngular();
        filterHolder.all(by.repeater('(key,bq) in drcData.drc.bands[$index].drcBqs.crossoverInstances')).count().then(function(count) {
            if (count === 0) {
                self.setCrossOverBand(filterName).then(function success() {
                    defer.fulfill();
                }, function error(reason) {
                    defer.reject(reason);
                })
            } else {
                var bandName = filterName.split(' ')[0];
                var filterIndex;
                self.setCrossOverBand(bandName).then(function success() {
                    self.setCrossOverFilter(filterHolder, filterName).then(function success() {
                        defer.fulfill();
                    })
                }, function error(reason) {
                    defer.reject(reason);
                })
            }
        })
        return defer.promise;
    },
    /**
     * sets the properties of crossover in basic mode.
     * @param property name of the property for which value has to be set.
     * @param bandName the name of the band (low,mid,high).
     * @param value value for the property to be set.
     */
    setCrossOverBasicProperty: function(property, bandName, value) {
        var isSet = false;
        element.all(by.repeater('(bandId, band) in drcData.drc.bands')).reduce(function(isPropertySet, bandContainer, index) {
            if (isPropertySet) {
                isSet = true;
                return isPropertySet;
            }
            return bandContainer.all(by.css('.crossover-tile-controls-left-title')).first().getText().then(function(name) {
                if (name.toLowerCase() === bandName.toLowerCase()) {
                    bandContainer.all(by.css('.nw-drc-gdata-label')).reduce(function(labelFound, label, labelIndex) {
                        if (labelFound) {
                            return labelFound;
                        }
                        return label.isDisplayed().then(function(displayed) {
                            if (displayed) {
                                label.getText().then(function(labelTitle) {
                                    if (labelTitle.toLowerCase() === property.toLowerCase()) {
                                        bandContainer.all(by.css('.nw-drc-gdata-input-ctrl')).get(labelIndex).clear();
                                        bandContainer.all(by.css('.nw-drc-gdata-input-ctrl')).get(labelIndex).sendKeys(value);
                                        return true;
                                    }
                                })
                            }
                        })
                    })
                    return true;
                }
            })
        }).then(function() {
            if (!isSet) {
                console.log('Skipped : ' + property + ' not present. So its value is not set to ' + value);
            }
        })
    },
    /**
     * changes the crossover property
     * @param testCase testcase to change the values.
     */
    changeCrossOverProperty: function(testCase) {
        var self = module.exports;
        var defer = protractor.promise.defer();
        testCase.crossOver.forEach(function(crossOver) {
            if (crossOver.mode === undefined) {
                crossOver.mode = 'adv';
            }
            self.selectCrossOverMode(crossOver.mode);
            if (crossOver.mode === 'basic') {
                var filterContainer = element(by.css('.nw-drc-right-inner-container.padd-top-zero'));
                self.setFilterType(filterContainer, crossOver.filterType).then(function() {
                    if (self.isKeyPresent(crossOver, 'bands')) {
                        crossOver.bands.forEach(function(band) {
                            if (!self.isKeyPresent(band, 'name')) {
                                band.name = "Low";
                            }
                            Object.keys(band).forEach(function(key) {
                                if (key !== 'name') {
                                    self.setCrossOverBasicProperty(key, band.name, band[key]);
                                }
                            });
                        })
                    }
                    defer.fulfill();
                })
            } else if (crossOver.mode === 'adv') {
                if (crossOver.filters !== undefined) {
                    crossOver.filters.forEach(function(filter) {
                        if (filter.name === undefined) {
                            filter.name = 'Low F1'
                        }
                        self.getFilterHolder().then(function(filterHolder) {
                            self.setFilter(filterHolder, filter.name).then(function success() {
                                self.setFilterType(filterHolder, filter.type).then(function() {
                                    Object.keys(filter).forEach(function(key) {
                                        if ((key !== 'name') && (key !== 'type')) {
                                            self.setCrossOverProperty(filterHolder, key, filter[key]);
                                            defer.fulfill();
                                        }
                                    })
                                })
                            }, function error() {
                                console.log('Skipped : ' + filter.name + ' is not present. So properties are not set.')
                            })
                        })
                    })
                }
                defer.fulfill();
            }
        })
        return defer.promise;
    }
}
var fs = require('fs');
var path = require('path');
var commonUtils = require('../commonUtils.js');
var drcUtils = require('./drcUtils.js');
var chai = require('chai');
var expect = chai.expect;

module.exports = {
    /**
     * Tests the DRC.
     * @param rootPath Current working directory path.
     * @param runInput Contents of config.json file.
     * @param audioPlayer audio player component to call its functions.
     * @param currentConfig Name of the configurations which is currently opened.
     * @param currentAudioMode Name of the audio mode which is currently opened.
     */

    test: function(rootPath, runInput, audioPlayer, currentConfig, currentAudioMode) {
        var defer = protractor.promise.defer();
        var isComponentPresent = false;
        var isNotCombined = false,
            componentIndex;
        it('should turn on DRC', function() {
            commonUtils.isSpeakerNotCombined("DRC").then(function() {
                isNotCombined = true;
            }, function() {})
            commonUtils.switchOnComponent("DRC").then(function(index) {
                isComponentPresent = true;
                commonUtils.openComponent(index);
            }, function() {}).then(function() {
                expect(isComponentPresent, 'DRC not present').to.be.true;
            })
        })
        var testInputs = require('./drc_testcases.json');
        testCases = commonUtils.findTestCasesToRun(testInputs.testCases, runInput, "drc");
        testCases.forEach(function(testCase, index) {
            it("should write test case " + testCase.id, function() {
                expect(isComponentPresent, 'DRC not present').to.be.true;
                if (isComponentPresent) {
                    drcUtils.turnOnDrc();
                    drcUtils.applyTestCase(testCase);
                    commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                        var deviceType;
                        if (isNotCombined) {
                            deviceType = 'Woofer_A';
                        } else {
                            deviceType = 'A';
                        }
                        commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, deviceType, runInput.appToTest);
                    });
                    commonUtils.applySnapshot();
                    commonUtils.isDeviceBAvailable().then(function() {
                        commonUtils.switchDevice('B');
                        drcUtils.turnOnDrc();
                        drcUtils.applyTestCase(testCase);
                        commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                            commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'B', runInput.appToTest);
                        });
                        commonUtils.applySnapshot();
                        commonUtils.switchDevice('A');
                    }, function(error) {}).then(function() {
                        if (isNotCombined) {
                            commonUtils.goBackToAudioProcessingPage();
                            commonUtils.switchToSpeaker('Tweeter');
                            commonUtils.switchOnComponent("DRC").then(function(index) {
                                componentIndex = index;
                                commonUtils.openComponent(index);
                                drcUtils.applyTestCase(testCase);
                                commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                                    commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'Tweeter_A', runInput.appToTest);
                                });
                                commonUtils.applySnapshot();
                                commonUtils.goBackToAudioProcessingPage();
                                commonUtils.switchToSpeaker('Woofer');
                                commonUtils.openComponent(index);
                            }, function() {})
                        }
                    }).then(function() {
                        if (index === testCases.length - 1) {
                            commonUtils.goBackToAudioProcessingPage();
                            defer.fulfill();
                        }
                    })
                }
            })
        })
        return defer.promise;
    }
}
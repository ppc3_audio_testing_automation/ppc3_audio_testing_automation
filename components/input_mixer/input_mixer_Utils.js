/**
 * This file has the funcitonalities of inputMixer as a functions.
 */
var shortDisplay = require('../abbreviations.json');
var common_utils = require('../commonUtils.js');
var fs = require('fs');
var loopAsync = require('../lib/loopAsync.js');
var chai = require('chai');
var expect = chai.expect;
var dom_inputMixer, rootPath, speakerType;
module.exports = {
    "applyChannelInput": function(channelInputs, dom_of_the_component) {
        var defer = protractor.promise.defer();
        loopAsync(channelInputs.length, function(loop) {
                browser.wait(function() {
                    return dom_of_the_component.all(by.css('.ocross-basic-ddown-holder')).get(loop.iteration()).isPresent();
                }, 20000, 'waiting for ddown-holder button');
                var skiptest = true;
                dom_of_the_component.all(by.css('.ocross-basic-ddown-holder')).get(loop.iteration()).all(by.tagName('option')).each(function(options) {
                    options.getText().then(function(optionLabel) {
                        if (optionLabel === channelInputs[loop.iteration()]) {
                            options.click().then(function() {
                                skiptest = false;
                            });
                        }
                    });
                }).then(function() {
                    if (skiptest) {
                        loop.break();
                        defer.reject();
                    } else {
                        loop.next();
                    }
                });
            },
            function() {
                defer.fulfill();
            });
        return defer.promise;
    },
    /**
     * This function applies the input based on the channel
     */
    "applyInput": function(testCaseObject) {
        var defer = protractor.promise.defer();
        var channelInputs = [];
        common_utils.switchOnComponent('Input Mixer').then(function(componentIndex) {
            dom_inputMixer = element.all(by.css('.grid_innerdiv')).get(componentIndex);
        }).then(function() {
            if (testCaseObject.basic.rightChannelOut === undefined) {
                channelInputs = [testCaseObject.basic.leftChannelOut];
            }
            if (testCaseObject.basic.rightChannelOut !== undefined) {
                channelInputs = [testCaseObject.basic.leftChannelOut, testCaseObject.basic.rightChannelOut];
            }
            module.exports.applyChannelInput(channelInputs, dom_inputMixer).then(function() {
                defer.fulfill();
            }, function() {
                defer.reject();
            });
        });
        return defer.promise;
    },
    /**
     * This checks the advanced page inputs whether the changes made in basic page or replicated or not
     */
    "checkAdvancedPage": function(testCaseObject) {
        var defer = protractor.promise.defer();
        module.exports.changeMode('advanced', dom_inputMixer, 'two-way-switch-holder').then(function() {
            element(by.model('data.mixerData.props.L2L.value.gain')).getAttribute('value').then(function(value) {
                expect(value).to.equal(testCaseObject.advanced.L2L);
            });
            element(by.model('data.mixerData.props.R2L.value.gain')).getAttribute('value').then(function(value) {
                expect(value).to.equal(testCaseObject.advanced.R2L);
            });
            element(by.model('data.mixerData.props.L2R.value.gain')).getAttribute('value').then(function(value) {
                expect(value).to.equal(testCaseObject.advanced.L2R);
            });
            element(by.model('data.mixerData.props.R2R.value.gain')).getAttribute('value').then(function(value) {
                expect(value).to.equal(testCaseObject.advanced.R2R);
            });
        });
        module.exports.changeMode('basic', dom_inputMixer, 'two-way-switch-holder', 'accept-restore-default').then(function() {
            defer.fulfill();
        });
        return defer.promise;
    },
    "changeMode": function(mode, dom_element_of_the_component, element_locator, ok_button_locator) {
        var defer = protractor.promise.defer();
        browser.wait(function() {
            return dom_element_of_the_component.all(by.css('.' + element_locator)).get(1).isPresent();
        }, 20000, 'waiting for two-way-switch-holder button');
        if (mode === 'basic') {
            dom_element_of_the_component.all(by.css('.' + element_locator)).get(0).click().then(function() {
                browser.wait(function() {
                    return dom_element_of_the_component.all(by.css('.' + ok_button_locator)).get(0).isPresent();
                }, 20000, 'waiting for the popup in the input mixer page');
                dom_element_of_the_component.all(by.css('.' + ok_button_locator)).get(0).click().then(function() {
                    browser.sleep(500).then(function() {
                        defer.fulfill();
                    });
                });
            });
        } else if (mode === 'advanced') {
            dom_element_of_the_component.all(by.css('.' + element_locator)).get(1).click().then(function() {
                defer.fulfill();
            });
        }
        return defer.promise;
    },
    "splitTestCases": function(testCasesJson) {
        var testcase_obj = {
            testcases_for_mono: [],
            testcases_for_stereo: []
        };
        testCasesJson.testCases.forEach(function(testcase) {
            if (testcase.basic.rightChannelOut !== undefined) {
                testcase_obj.testcases_for_stereo.push(testcase);
            } else if (testcase.basic.rightChannelOut === undefined) {
                testcase_obj.testcases_for_mono.push(testcase);
            }
        });
        return testcase_obj;
    }
};
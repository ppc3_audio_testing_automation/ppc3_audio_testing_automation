/**
 * This file has the test specs for the inputMixer component
 */

var testCasesJson = require('./input_mixer_testcases.json'); //Retrieving the testcases of inputMixer from a JSON file 
var input_mixer_utils = require('./input_mixer_Utils.js'); //Importing the functions of inputMixer
var common_utils = require('../commonUtils.js');
var audio_player = require('../audio_player/audio_player_operations.js');
var path = require('path');
var configuration;

module.exports = {
    test: function(rootPath, runInput, audioPlayer, config_name, audioModeElement) {
        /**
         * This spec is to check the basic default values of the inputMixer.
         * getExpectedValues function retrieves the default values from the .ppc3 file of that particular processFlow
         * The generated JSON file while recording commonAudioInput is dumped into the reference_audio folder
         * checkBasicDefaults function checks the basic page default values of the current process flow
         * If deviceB is there it repeats the same testing process by using expectedValuesOfInputMixerDeviceB[]
         */
        var defer = protractor.promise.defer();
        /**
         * Getting the testcases from the JSON file based on the deviceType (Mono or Stereo)
         */
        var testCases;
        testCases = input_mixer_utils.splitTestCases(testCasesJson);
        if (config_name.indexOf('Stereo') > -1) {
            testCases = common_utils.findTestCasesToRun(testCases.testcases_for_stereo, runInput, "input_mixer");
        } else if (config_name.indexOf('Mono') > -1) {
            testCases = common_utils.findTestCasesToRun(testCases.testcases_for_mono, runInput, "input_mixer");
        }
        // Iterating through the testcases 
        testCases.forEach(function(testCaseObject, posit) {
            /**
             * This spec is to apply the testcases
             * applyInput() - applies the input values
             * checkAdvancedPage() - checks the advanced page whether the changes are replicated or not
             * recordAudio() - uploads the audio of the particular testcase and records it
             * dumpAudioFile() - dumps the recorded audio(.json) from the source to the reference_audio
             */
            it(testCaseObject.id, function() {
                browser.ignoreSynchronization = true;
                input_mixer_utils.applyInput(testCaseObject).then(function() {
                    input_mixer_utils.checkAdvancedPage(testCaseObject).then(function() {
                        common_utils.recordAudio(rootPath, audio_player, testCaseObject, testCasesJson.defaultAudioInput).then(function() {
                            common_utils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCaseObject.id, config_name, audioModeElement, 'A', runInput.appToTest);
                            common_utils.applySnapshot();
                        });
                    });
                }).then(function() {
                    common_utils.isDeviceBAvailable().then(function() {
                        common_utils.switchDevice('B');
                        input_mixer_utils.applyInput(testCaseObject).then(function() {
                            input_mixer_utils.checkAdvancedPage(testCaseObject).then(function() {
                                common_utils.recordAudio(rootPath, audio_player, testCaseObject, testCasesJson.defaultAudioInput).then(function() {
                                    common_utils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCaseObject.id, config_name, audioModeElement, 'B', runInput.appToTest);
                                    common_utils.applySnapshot();
                                });
                            });
                        });
                        common_utils.switchDevice('A');
                    }, function() {});
                });
            });
        });
        return defer.promise;
    }
};
var until = protractor.ExpectedConditions;
var fs = require('fs');
var abbr = require('./abbreviations.json');
var waitAbsent = require('./lib/waitAbsent.js');
var path = require('path');
module.exports = {
    "findTestCasesToRun": function(testInputsTestcases, testConfigJson, componentName) {
        var testCases = [];
        if (Object.keys(testConfigJson).indexOf("testCasesToRun") > -1) {
            if (Object.keys(testConfigJson.testCasesToRun).indexOf(componentName) > -1) {
                if (testConfigJson.testCasesToRun[componentName].length === 0) {
                    testCases = testInputsTestcases;
                } else {
                    testInputsTestcases.forEach(function(testCase) {
                        if (testConfigJson.testCasesToRun[componentName].indexOf(testCase.id) > -1) {
                            testCases.push(testCase);
                        }
                    })
                }
            } else {
                testCases = testInputsTestcases;
            }
        } else {
            testCases = testInputsTestcases;
        }
        return testCases;
    },
    "switchDevice": function(device) {
        element.all(by.css('div[title="Click to tune and configure device ' + device + '"]')).first().isDisplayed().then(function(displayed) {
            if (displayed) {
                element.all(by.css('div[title="Click to tune and configure device ' + device + '"]')).first().click();
                browser.wait(function() {
                    return element.all(by.css('div[title="Click to tune and configure device ' + device + '"]')).first().waitAbsent();
                }, 10000, "waiting for device " + device + " icon to disappear");
            }
        });
    },
    "recordAudio": function(rootPath, audioPlayer, testCase, defaultAudioInput) {
        var defer = protractor.promise.defer();
        var audioFile;
        if (testCase.audioFile === undefined || testCase.audioFile === '') {
            if (defaultAudioInput !== undefined) {
                audioFile = defaultAudioInput;
            }
        } else {
            audioFile = testCase.audioFile;
        }
        if (!fs.existsSync(audioFile)) {
            console.log("Audio File not present : ", testCase.audioFile);
        }
        if (audioFile !== undefined) {
            audioPlayer.openAudioPlayer().then(function() {
                audioPlayer.uploadAudio(audioFile).then(function() {
                    audioPlayer.recordAudio().then(function() {
                        audioPlayer.closeAudioPlayer().then(function() {
                            defer.fulfill();
                        });
                    });
                });
            });
        }
        return defer.promise;
    },
    "switchChannel": function(channel) {
        var defer = protractor.promise.defer();
        if (channel === 'right')
            element(by.id('channelSwitcher')).all(by.css('.loglinear-holder')).get(1).click().then(function() {
                browser.sleep(1000).then(function() {
                    defer.fulfill();
                });
            });
        else if (channel === 'left')
            element(by.id('channelSwitcher')).all(by.css('.loglinear-holder')).get(0).click().then(function() {
                browser.sleep(1000).then(function() {
                    defer.fulfill();
                });
            });
        return defer.promise;
    },
    "dumpAudioFile": function(sourcePath, destPath, fileName, config, processFlow, deviceType, appName) {
        var date = new Date();
        if (!fs.existsSync(destPath + "/audio_files/recorded_audio")) {
            fs.mkdirSync(destPath + "/audio_files/recorded_audio");
        }
        var testdata = JSON.parse(fs.readFileSync(sourcePath + '/node_modules/ti_addonmgr/node_modules/' + appName + '/app/plugins/' + appName + '/testData.json').toString());
        testdata.inputAudioPath = path.normalize(destPath + '/audio_files/input_audio');
        testdata.recordedAudioPath = path.normalize(destPath + '/audio_files/recorded_audio');
        testdata.referenceAudioPath = path.normalize(destPath + '/audio_files/reference_audio');
        fs.writeFileSync(destPath + '/audio_files/recorded_audio/' + fileName + '_' + config.replace('/', '') + '_' + abbr.processFlows[processFlow] + '_' + deviceType + '_' + date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear() + '_' + date.getHours() + 'h' + '-' + date.getMinutes() + 'm' + '-' + date.getSeconds() + 's' + '.json', JSON.stringify(testdata));
    },
    "openComponent": function(componentIndex) {
        var defer = protractor.promise.defer();
        element.all(by.css('.grid_innerdiv')).get(componentIndex).all(by.css('.zoom_button')).first().click().then(function() {
            browser.sleep(2000).then(function() {
                defer.fulfill();
            });
        });
        return defer.promise;
    },
    "takeSnapshot": function() {
        var defer = protractor.promise.defer();
        browser.wait(function() {
            return element.all(by.css('.snap-camera-holder')).first().isDisplayed();
        }, 50000, 'waiting for camera to be visible');
        element.all(by.css('.snap-camera-holder')).first().click().then(function() {
            defer.fulfill();
        });
        return defer.promise;
    },
    "applySnapshot": function() {
        var defer = protractor.promise.defer();
        element.all(by.repeater('snap in snapshot.snapshots() | snapshotFilter')).first().isDisplayed().then(function(displayed) {
            if (displayed) {
                element.all(by.repeater('snap in snapshot.snapshots() | snapshotFilter')).first().click();
                defer.fulfill();
            }
        });
        return defer.promise;
    },
    "resetTuning": function() {
        var defer = protractor.promise.defer();
        browser.wait(function() {
            return element.all(by.css('.aup-settings-ddown-holder')).first().isDisplayed();
        }, 50000, 'waiting for the ddown-holder button');
        element.all(by.css('.aup-settings-ddown-holder')).first().click().then(function() {
            element.all(by.css('.selected-hflow-ddown')).get(1).all(by.css('.hflow-ddown-options')).each(function(dom) {
                dom.getText().then(function(text) {
                    if (text === "Reset Tuning") {
                        dom.click().then(function() {
                            browser.wait(function() {
                                return element.all(by.css('.RestoreDefaultPopUp')).first().all(by.css('.accept-restore-default')).first().isDisplayed();
                            }, 50000, 'waiting for the popup for restoring the default');
                            element.all(by.css('.RestoreDefaultPopUp')).first().all(by.css('.accept-restore-default')).first().click().then(function() {
                                var evm_overlay;
                                evm_overlay = element(by.css('.evm-pup-progress-container'));
                                browser.wait(function() {
                                    return evm_overlay.isDisplayed();
                                }, 50000, 'waiting ');
                                browser.sleep('1000');
                                browser.wait(function() {
                                    return evm_overlay.waitAbsent();
                                }, 50000, 'waiting to close the evm overlay').then(function() {
                                    defer.fulfill();
                                });
                            });
                        });
                    }
                });
            });
        });
        return defer.promise;
    },
    "deleteSnapshot": function() {
        var defer = protractor.promise.defer();
        element.all(by.repeater('snap in snapshot.snapshots() | snapshotFilter')).first().isDisplayed().then(function(displayed) {
            if (displayed) {
                browser.executeScript("document.getElementById('snapshotMainTooltip').setAttribute('class','sshot-tt-tooltip-container')");
                browser.wait(function() {
                    return element.all(by.css('.snap-tootip-delete-icon')).first().isDisplayed();
                }, 50000, 'waiting for the delete button');
                element.all(by.css('.snap-tootip-delete-icon')).first().click().then(function() {
                    browser.executeScript("document.getElementById('snapshotMainTooltip').setAttribute('class','sshot-tt-tooltip-container ng-hide')");
                    defer.fulfill();
                });
            }
        });
        return defer.promise;
    },
    "switchOffComponent": function(componentName) {
        var defer = protractor.promise.defer();
        browser.wait(function() {
            return element.all(by.css('.grid_innerdiv')).first().isPresent();
        }, 50000, 'waiting for the innerdiv');
        element.all(by.css('.grid_innerdiv')).each(function(dom, index) {
            dom.all(by.css('.tile-headings-common')).reduce(function(result, title_dom, title_index) {
                if (result)
                    return result;
                return title_dom.getText().then(function(text) {
                    if (text.indexOf(componentName) > -1) {
                        dom.all(by.css('.on-switch')).get(0).isDisplayed().then(function(present) {
                            if (present)
                                dom.all(by.css('.on-switch')).get(0).click().then(function() {
                                    return dom;
                                });
                        });
                    }
                });
            });
        }).then(function() {
            defer.fulfill();
        });
        return defer.promise;
    },
    "switchOnComponent": function(componentName) {
        var defer = protractor.promise.defer();
        var index_of_the_component;
        element.all(by.css('.grid_innerdiv')).reduce(function(result1, dom, index) {
            if (result1 > -1)
                return result1;
            return dom.all(by.css('.tile-headings-common')).reduce(function(result, title_dom, title_index) {
                if (result > -1)
                    return result;
                return title_dom.getText().then(function(text) {
                    if (text.indexOf(componentName) > -1) {
                        index_of_the_component = index;
                        return dom;
                    }
                });
            });
        }).then(function() {
            if (index_of_the_component !== undefined) {
                element.all(by.css('.grid_innerdiv')).get(index_of_the_component).all(by.css('.off-switch')).first().isPresent().then(function(present) {
                    if (present) {
                        element.all(by.css('.grid_innerdiv')).get(index_of_the_component).all(by.css('.off-switch')).first().isDisplayed().then(function(displayed) {
                            if (displayed) {
                                element.all(by.css('.grid_innerdiv')).get(index_of_the_component).all(by.css('.off-switch')).first().click();
                            }
                        })
                    }
                });
                defer.fulfill(index_of_the_component);
            } else {
                defer.reject('Component not present');
            }
        });
        return defer.promise;
    },
    "goBackToAudioProcessingPage": function() {
        element.all(by.css('.breadscrum-link')).get(2).click();
        browser.wait(function() {
            return element.all(by.css('.breadscrum-link')).get(3).waitAbsent();
        }, 50000, 'waiting for the 4th link to disappear');
    },
    "isDeviceBAvailable": function() {
        var defer = protractor.promise.defer();
        element.all(by.id('deviceSelectControl')).first().isDisplayed().then(function(displayed) {
            if (displayed) {
                defer.fulfill();
            } else {
                defer.reject('Device B not present');
            }
        });
        return defer.promise;
    },
    /**
     * Checks whether the component's speaker is combined or not.
     * @param componentName name of the component as in the UI.
     * The promise gets fulfilled if the component is separate for tweeter and woofer(not combined).
     * The promise gets rejected if the component is common.(combined speaker).
     */
    "isSpeakerNotCombined": function(componentName) {
        var defer = protractor.promise.defer();
        var tileIndex;
        element(by.id('rb-grid-left')).isPresent().then(function(displayed) {
            if (displayed) {
                element(by.id('rb-grid-left')).all(by.css(".headsection")).reduce(function(result, elm, index) {
                    if (result > -1) {
                        return result;
                    }
                    return elm.all(by.css(".tile-headings-common")).first().getText().then(function(text) {
                        if (text === componentName) {
                            tileIndex = index;
                            return tileIndex;
                        }
                    });
                }).then(function(tileIndex) {
                    if (tileIndex === undefined) {
                        defer.fulfill();
                    }
                })
            } else {
                defer.reject();
            }
        })
        return defer.promise;
    },
    /**
     * switches to specified speaker type(Woofer or Tweeter).
     * @param speakerType name of speaker(Woofer or Tweeter).
     */
    "switchToSpeaker": function(speakerType) {
        element.all(by.css(".ap-main-tab")).each(function(element, index) {
            element.getText().then(function(text) {
                if (text === speakerType) {
                    element.click();
                    // browser.sleep(1000) //required for transition
                    browser.wait(function() {
                        return element.all(by.css(".ap-main-tab")).get(index).waitAbsent();
                    }, 10000, "Waiting for " + speakerType + " to disappear");
                }
            });
        });
    }
};
var fs = require('fs');
var path = require('path');
var commonUtils = require('../commonUtils.js');
var pvddUtils = require('./pvdd_utils.js');
var chai = require('chai');
var expect = chai.expect;

module.exports = {
    /**
     * Tests the PVDD.
     * @param rootPath Current working directory path.
     * @param runInput Contents of config.json file.
     * @param audioPlayer audio player component to call its functions.
     * @param currentConfig Name of the configurations which is currently opened.
     * @param currentAudioMode Name of the audio mode which is currently opened.
     */

    test: function(rootPath, runInput, audioPlayer, currentConfig, currentAudioMode) {
        var defer = protractor.promise.defer();
        var isComponentPresent = false;
        var isNotCombined = false,
            componentIndex;
        it('should open PVDD', function() {
            commonUtils.isSpeakerNotCombined("PVDD").then(function() {
                isNotCombined = true;
            }, function() {})
            pvddUtils.openPvdd().then(function() {
                isComponentPresent = true;
            }, function() {}).then(function() {
                expect(isComponentPresent, 'PVDD not present').to.be.true;
            })
        })
        var testInputs = require('./pvdd_testcases.json');
        testCases = commonUtils.findTestCasesToRun(testInputs.testCases, runInput, "drc");
        testCases.forEach(function(testCase, index) {
            it("should write test case " + testCase.id, function() {
                expect(isComponentPresent, 'PVDD not present').to.be.true;
                if (isComponentPresent) {
                    pvddUtils.applyTestCase(testCase);
                    commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                        var deviceType;
                        if (isNotCombined) {
                            deviceType = 'Woofer_A';
                        } else {
                            deviceType = 'A';
                        }
                        commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, deviceType, runInput.appToTest);
                    });
                    commonUtils.applySnapshot();
                    commonUtils.isDeviceBAvailable().then(function() {
                        commonUtils.switchDevice('B');
                        pvddUtils.applyTestCase(testCase);
                        commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                            commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'B', runInput.appToTest);
                        });
                        commonUtils.applySnapshot();
                        commonUtils.switchDevice('A');
                    }, function(error) {}).then(function() {
                        if (isNotCombined) {
                            commonUtils.goBackToAudioProcessingPage();
                            commonUtils.switchToSpeaker('Tweeter');
                            pvddUtils.openPvdd().then(function(index) {
                                componentIndex = index;
                                pvddUtils.applyTestCase(testCase);
                                commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                                    commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'Tweeter_A', runInput.appToTest);
                                });
                                commonUtils.applySnapshot();
                                commonUtils.goBackToAudioProcessingPage();
                                commonUtils.switchToSpeaker('Woofer');
                            }, function() {})
                        }
                    }).then(function() {
                        if (index === testCases.length - 1) {
                            commonUtils.goBackToAudioProcessingPage();
                            defer.fulfill();
                        }
                    })
                }
            })
        })
        return defer.promise;
    }
}
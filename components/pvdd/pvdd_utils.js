module.exports = {
    /**
     * Sets the house keeping controls.
     * @param property name of the control.
     * @param value value to be set for the property.
     */
    "setHouseKeepingControl": function(property, value) {
        element(by.id('biquad-settings-holder')).all(by.css('.nominal-loudness-field-titles')).reduce(function(isValueSet, headingHolder, headingIndex) {
            if (isValueSet) {
                return;
            }
            return headingHolder.getText().then(function(heading) {
                if (heading.toLowerCase() === property.toLowerCase()) {
                    element(by.id('biquad-settings-holder')).all(by.css('.nw-gdata-input-ctrl')).get(headingIndex).clear();
                    element(by.id('biquad-settings-holder')).all(by.css('.nw-gdata-input-ctrl')).get(headingIndex).sendKeys(value);
                    return true;
                }
            })
        })
    },
    /**
     * Applies the test case.
     * @param testCase test case object.
     */
    "applyTestCase": function(testCase) {
        var self = this;
        element(by.id("pvdd-settings-control-trigger")).click();
        browser.wait(function() {
            return element(by.id('biquad-settings-holder')).isDisplayed();
        }, 2000, 'Waiting for biquad settings holder.');
        Object.keys(testCase).forEach(function(property) {
            if (property !== 'id') {
                self.setHouseKeepingControl(property, testCase[property]);
            }
        })
    },
    /**
     * Opens PVDD component.
     */
    "openPvdd": function() {
        var defer = protractor.promise.defer();
        var index_of_the_component;
        element.all(by.css('.grid_innerdiv')).reduce(function(holderIndexGot, dom, holderIndex) {
            if (holderIndexGot > -1)
                return holderIndexGot;
            return dom.all(by.css('.tile-headings-common')).reduce(function(titleIndexGot, title_dom, title_index) {
                if (titleIndexGot > -1)
                    return tileIndexGot;
                return title_dom.getText().then(function(text) {
                    if (text.indexOf('PVDD') > -1) {
                        index_of_the_component = holderIndex;
                        return title_index;
                    }
                });
            });
        }).then(function() {
            if (index_of_the_component !== undefined) {
                element.all(by.css('.grid_innerdiv')).get(index_of_the_component).all(by.css('.zoom_button')).first().click().then(function() {
                    browser.wait(function() {
                        return element(by.id("pvdd-settings-control-trigger")).isDisplayed();
                    }, 50000, 'Waiting for pvdd page.');
                    defer.fulfill();
                });
            } else {
                defer.reject('Component not present');
            }
        });
        return defer.promise;
    }
}
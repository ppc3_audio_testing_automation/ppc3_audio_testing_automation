module.exports = {
    "openAudioPlayer": function() {
        var defer = protractor.promise.defer();
        element.all(by.css('.snaps-heading-holder')).each(function(holder, index) {
            holder.getText().then(function(text) {
                if (text === "Audio Player") {
                    holder.click();
                    defer.fulfill();
                }
            })
        })
        return defer.promise;
    },
    "makeRecordButtonVisible": function() {
        var defer = protractor.promise.defer();
        browser.sleep(500);
        browser.actions().dragAndDrop(element(by.css('.apl-tab-container')), { x: -20, y: 50 }).mouseUp().perform();
        var noOfClicks = [1, 2, 3, 4, 5, 6];
        noOfClicks.filter(function(ele) {
            element(by.css('li[title="Switch to audio player tab"]')).click();
            if (ele === 6) {
                defer.fulfill();
            }
        });
        return defer.promise;
    },
    "uploadAudio": function(audioFilePath) {
        var defer = protractor.promise.defer();
        browser.sleep(700);
        browser.wait(function() {
            return element(by.css('.apl-tab-container')).isDisplayed();
        }, 20000, 'waiting for tab element');
        element.all(by.repeater('tracks in audioPlayerData.trackList')).each(function(tracks, trackPosition) {
            tracks.all(by.css('.apl-play-remove-btn')).get(trackPosition).click();
        });
        browser.sleep(500);
        element(by.css('input[name="WaveFile"]')).sendKeys(audioFilePath).then(function() {
            browser.wait(function() {
                return element(by.css('.apl-inner-container')).all(by.css('.file-menu-loader')).get(0).waitAbsent();
            }, 50000, 'waiting for loader');
        });
        browser.wait(function() {
            return element(by.css('.apl-play-loop-btn')).isDisplayed();
        }, 20000, 'waiting for the loop button');
        element(by.css('.apl-play-loop-btn')).click().then(function() {
            defer.fulfill();
        });
        defer.fulfill();
        return defer.promise;
    },
    "recordAudio": function() {
        var defer = protractor.promise.defer();
        browser.wait(function() {
            return element(by.css('.apl-record-btn.ng-scope.stoprecording')).isDisplayed();
        }, 20000, "Waiting for record button to appear");
        element(by.css('.apl-record-btn.ng-scope.stoprecording')).isDisplayed().then(function(displayed) {
            element(by.css('.apl-record-btn.ng-scope.stoprecording')).click().then(function() {
                defer.fulfill();
            });
        });
        return defer.promise;
    },
    "closeAudioPlayer": function() {
        var defer = protractor.promise.defer();
        browser.sleep(500);
        element(by.css('.snap-popup-container')).all(by.css('.snap-popup-close')).get(0).click().then(function() {
            browser.sleep('300');
            defer.fulfill();
        });
        return defer.promise;
    }
};
var runInput = require('../../config.json');
var navigate_Utils = require('./navigate_Utils.js');
var chai = require('chai');
var waitAbsent = require('../lib/waitAbsent.js');
var expect = chai.expect;
var path = require('path');
var processFlowPosition = {}; //This is to hold the process flow and its positions
var rootPath;
module.exports = {
    "navigateUpToTunningPage": function(configElement, pos, runInput) {
        navigate_Utils.chooseTheApp(rootPath, runInput);
        navigate_Utils.chooseTheConfiguration(configElement);
        navigate_Utils.chooseTheHomePageContent().then(function() {
            processFlowPosition = {};
            browser.sleep(2000);
            browser.wait(function() {
                return element.all(by.repeater('(key, value) in hybridFlowInfo')).get(0).isDisplayed();
            }, 50000, 'waiting for the select button');
            element.all(by.repeater('(key, value) in hybridFlowInfo')).each(function(selecthybridbtn, position) {
                var processFlowName = '';
                selecthybridbtn.all(by.css('.ng-binding')).each(function(paragraph) {
                    paragraph.getText().then(function(text) {
                        processFlowName += text + ' ';
                    });
                }).then(function() {
                    var obj = {};
                    processFlowName = processFlowName.substring(0, processFlowName.length - 1);
                    processFlowPosition[processFlowName] = position;
                });
            });
        });
    },
    "navigateThroughAudioModes": function(configElement, audioModeElement) {
        var defer = protractor.promise.defer();
        navigate_Utils.chooseTheProcessFlow(processFlowPosition, audioModeElement);
        browser.sleep(600);
        // navigate_Utils.savePPC3File(configElement).then(function() {
        defer.fulfill();
        // });
        return defer.promise;
    },
    "changeAudioMode": function(audioModeElement) {
        var defer = protractor.promise.defer();
        browser.sleep('1000');
        browser.wait(function() {
            return element(by.css('.aup-settings-ddown-holder')).isDisplayed();
        }, 50000, 'waiting for settings button');
        element(by.css('.aup-settings-ddown-holder')).click();
        browser.sleep('1000');
        browser.wait(function() {
            return element.all(by.css('.hflow-ddown-options')).get(3).isDisplayed();
        }, 50000, 'waiting for ddown options');
        browser.sleep('1000');
        element.all(by.css('.hflow-ddown-options')).get(3).click().then(function() {
            defer.fulfill();
        });
        return defer.promise;
    },
    "navigateBackToHomePage": function() {
        var defer = protractor.promise.defer();
        // browser.getCurrentUrl().then(function(url) {
        //     expect(path.basename(url)).to.equal('audioProcessing');
        // });
        browser.wait(function() {
            return element(by.css('.popup-close')).isDisplayed();
        }, 50000, 'waiting for popup close button');
        element(by.css('.popup-close')).click();
        browser.getCurrentUrl().then(function(url) {
            expect(path.basename(url)).to.equal('audioProcessing');
        });
        browser.wait(function() {
            return element(by.css('.popup-close')).waitAbsent();
        }, 50000, 'waiting for popup to close');
        element.all(by.css('.breadscrum-link')).get(0).click().then(function() {
            browser.sleep('1000');
            browser.getCurrentUrl().then(function(url) {
                expect(path.basename(url)).to.equal('audioProcessing');
            });
            element.all(by.css('.imp-brj-btn.save-current')).get(1).click().then(function() {
                browser.getCurrentUrl().then(function(url) {
                    expect(path.basename(url)).to.equal('audioProcessing');
                });
            });
        });
        browser.getAllWindowHandles().then(function(handles) {
            browser.switchTo().window(handles[0]);
            browser.getCurrentUrl().then(function(url) {
                expect(path.basename(url)).to.equal('appcenter');
            });
            browser.sleep(500);
            defer.fulfill();
        });
        return defer.promise;
    },
    "resolvePopup": function(path) {
        rootPath = path;
        var defer = protractor.promise.defer();
        browser.sleep(2000);
        browser.ignoreSynchronization = true;
        var ele = element(by.css('.uncaughtException-popup-container'));
        ele.isDisplayed().then(function(displayed) {
            if (displayed) {
                element(by.css('.no-btn-uncaughtException')).click().then(function() {
                    defer.fulfill();
                });
            } else {
                defer.fulfill();
            }
        });
        return defer.promise;
    }
};
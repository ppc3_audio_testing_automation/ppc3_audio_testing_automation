var fs = require('fs');
var path = require('path');
var clipper = require('./clipperUtils.js');
var commonUtils = require('../commonUtils.js');

module.exports = {
    /**
     * Tests the clipper.
     * @param rootPath Current working directory path.
     * @param runInput Contents of config.json file.
     * @param audioPlayer audio player component to call its functions.
     * @param currentConfig Name of the configurations which is currently opened.
     * @param currentAudioMode Name of the audio mode which is currently opened.
     */
    test: function(rootPath, runInput, audioPlayer, currentConfig, currentAudioMode) {
        var defer = protractor.promise.defer();
        var testInputs = require('./clipper_testcases.json');
        var testCases = commonUtils.findTestCasesToRun(testInputs.testCases, runInput, "clipper");
        testCases.forEach(function(testCase, index) {
            it("should write test case : " + testCase.id, function() {
                clipper.applyTestCase(testCase);
                commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                    var deviceType;
                    commonUtils.isSpeakerNotCombined().then(function() {
                        deviceType = 'Woofer_A';
                    }, function() {
                        deviceType = 'A';
                    }).then(function() {
                        commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, deviceType, runInput.appToTest);
                        commonUtils.applySnapshot();
                    })
                })
                commonUtils.isDeviceBAvailable().then(function() {
                    commonUtils.switchDevice('B');
                    clipper.applyTestCase(testCase);
                    commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                        commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'B', runInput.appToTest);
                        commonUtils.applySnapshot();
                    })
                    commonUtils.switchDevice('A');
                }, function(error) {})
                commonUtils.isSpeakerNotCombined().then(function() {
                    commonUtils.switchToSpeaker('Tweeter');
                    clipper.applyTestCase(testCase);
                    commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                        commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'Tweeter_A', runInput.appToTest);
                        commonUtils.applySnapshot();
                    });
                    commonUtils.switchToSpeaker('Woofer');
                }, function(error) {})
                if (index === testCases.length - 1) {
                    defer.fulfill();
                }
            })
        })
        return defer.promise;
    }
}
var waitAbsent = require('../lib/waitAbsent.js');
var chai = require('chai');
var commonUtils = require('../commonUtils.js');
var expect = chai.expect;
var path = require('path');
module.exports = {
    /**
     * checks if the given key is present in the given object or not.
     * @param obj Object to find the keys in it.
     * @param key key to find its existence in the given object. 
     */
    isKeyPresent: function(obj, key) {
        if (Object.keys(obj).indexOf(key) > -1) {
            return true;
        }
        return false;
    },
    /**
     * Applies the test case.
     * @param testCase test case object to apply.
     */
    applyTestCase: function(testCase) {
        var self = module.exports;
        commonUtils.switchOnComponent('Clipper').then(function(componentIndex) {
            expect(componentIndex).to.be.above(-1);
        })
        if (self.isKeyPresent(testCase, 'makeUpGain')) {
            self.setMakeUpGain(testCase.makeUpGain);
        }
        if (self.isKeyPresent(testCase, 'clipperLevel')) {
            self.setClipperLevel(testCase.clipperLevel);
        }
    },
    /**
     * sets the make up gain.
     * @param value value to get applied to the make up gain.
     */
    setMakeUpGain: function(value) {
        browser.wait(function() {
            return element.all(by.css(".clipper-adv-trigger")).first().isDisplayed();
        }, 10000, "Trigger icon for make up gain taking too long to appear");
        element.all(by.css(".clipper-adv-trigger")).first().click();
        browser.wait(function() {
            return element(by.model("data.mixerData.props.fineVolume.value")).isDisplayed();
        }, 10000, "Make up gain holder taking too long to appear");
        element(by.model("data.mixerData.props.fineVolume.value")).clear();
        element(by.model("data.mixerData.props.fineVolume.value")).sendKeys(value);
        element.all(by.css(".custom-popup-container.thd")).each(function(elm, index) {
            elm.all(by.css(".custom-popup-title-holder")).first().getText().then(function(text) {
                if (text === "Advanced Clipper") {
                    element.all(by.css(".custom-popup-container.thd")).get(index).all(by.css(".snap-popup-close")).get(0).click();
                    browser.wait(function() {
                        return element.all(by.css(".custom-popup-container.thd")).get(index).all(by.css(".snap-popup-close")).get(0).isDisplayed();
                    }, 10000, 'make uo gain container taking too long to close');
                }
            })
        })
    },
    /**
     * sets the clipper level.
     * @param value value to get applied to the clipper level.
     */
    setClipperLevel: function(value) {
        var container = element.all(by.css(".input-field-holder.mgn-btm-zero.clipper")).first();
        browser.wait(function() {
            return container.all(by.css(".slider-value-holder-tw")).first().isDisplayed();
        }, 10000, "Clipper leven input field holder taking too long to appear");
        container.all(by.css(".slider-value-holder-tw")).first().click();
        browser.wait(function() {
            return container.all(by.css(".input-txt")).first().isDisplayed();
        }, 10000, "Input field for clipper level taking too long to appear");
        container.all(by.css(".input-txt")).first().clear();
        container.all(by.css(".input-txt")).first().sendKeys(value);
        container.all(by.css(".cc-nd-input-title")).first().click();
        browser.wait(function() {
            return container.all(by.css(".input-txt")).first().waitAbsent();
        }, 10000, "Clipper level text field taking too long to close");
    }

}
var waitAbsent = require('../lib/waitAbsent.js');
module.exports = {
    /**
     * Checks whether bass compensation panel is switched on or not,if not,it switches it on.
     * Resolves when bass compensation is switched on.
     */
    "isBassCompensationSwitchedOn": function() {
        var defer = protractor.promise.defer();
        var self = this;
        element.all(by.css('.tflow-flip-frontpanel')).reduce(function(panelIndexFound, panelHolder, panelIndex) {
            if (panelIndexFound > -1) {
                return panelIndexFound;
            }
            return panelHolder.isDisplayed().then(function(displayed) {
                if (displayed) {
                    return panelHolder.all(by.css('.tw-panels-heading.tile-headings-common')).first().isPresent().then(function(present) {
                        if (present) {
                            return panelHolder.all(by.css('.tw-panels-heading.tile-headings-common')).first().getText().then(function(heading) {
                                if (heading.toLowerCase() === 'bass compensation') {
                                    return panelHolder.all(by.css('.off-switch')).first().isDisplayed().then(function(displayed) {
                                        if (displayed) {
                                            self.switchOnPanel('bass compensation').then(function() {
                                                defer.fulfill();
                                            }, function() {})
                                        } else {
                                            defer.fulfill();
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        });
        return defer.promise;
    },
    /**
     * Switches on the panel and returns the panel container.
     * @param panelName name of the panel.
     */
    "switchOnPanel": function(panelName) {
        var defer = protractor.promise.defer();
        var self = this;
        if (panelName.toLowerCase() === 'bass compensation') {
            browser.wait(function() {
                return element.all(by.css('.tflow-top-right-container')).first().isDisplayed();
            }, 50000, 'Waiting for component page to appear.');
            element.all(by.css('.enable-eq-default')).first().isPresent().then(function(present) {
                if (present) {
                    element.all(by.css('.enable-eq-default')).first().isDisplayed().then(function(displayed) {
                        if (displayed) {
                            element.all(by.css('.enable-eq-default')).first().click();
                            browser.wait(function() {
                                return element.all(by.css('.enable-eq-default')).first().waitAbsent();
                            }, 20000, 'Waiting for enable eq button to disappear.');
                        }
                    })
                }
            })
        } else if (panelName.toLowerCase() === 'bass compression') {
            self.isBassCompensationSwitchedOn().then(function() {});
        }
        element.all(by.css('.tflow-flip-frontpanel')).reduce(function(panelIndexFound, panelHolder, panelIndex) {
            if (panelIndexFound > -1) {
                return panelIndexFound;
            }
            return panelHolder.isDisplayed().then(function(displayed) {
                if (displayed) {
                    return panelHolder.all(by.css('.tw-panels-heading.tile-headings-common')).first().isPresent().then(function(present) {
                        if (present) {
                            return panelHolder.all(by.css('.tw-panels-heading.tile-headings-common')).first().getText().then(function(heading) {
                                if (heading.toLowerCase() === panelName.toLowerCase()) {
                                    return panelHolder.all(by.css('.off-switch')).first().isDisplayed().then(function(displayed) {
                                        if (displayed) {
                                            panelHolder.all(by.css('.off-switch')).first().click();
                                            browser.wait(function() {
                                                return panelHolder.all(by.css('.disable-overlay-ap-tile.disable-overlay-smartamp-bass-comp')).first().waitAbsent();
                                            }, 20000, 'Waiting for overlay to disappear in Smart Bass.');
                                            return panelIndex;
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }).then(function(panelIndexFound) {
            if (panelIndexFound > -1) {
                defer.fulfill(element.all(by.css('.tflow-flip-frontpanel')).get(panelIndexFound));
            } else {
                defer.reject(panelName + ' panel not found.')
            }
        })
        return defer.promise;
    },
    /**
     * Sets the value to the slider.
     * @param panelHolder the container for the panel in which value has to be set.
     * @param property name of the property to set the value.
     * @param value value to be set to the given property.
     */
    "setSliderValue": function(panelHolder, property, value) {
        var defer = protractor.promise.defer();
        panelHolder.all(by.css('.smartamp-slider-holder')).reduce(function(isFreqSet, sliderHolder) {
            if (isFreqSet) {
                return true;
            }
            return sliderHolder.all(by.css('.cc-nd-input-title')).first().isPresent().then(function(present) {
                if (present) {
                    return sliderHolder.all(by.css('.cc-nd-input-title')).first().getText().then(function(heading) {
                        if (heading.toLowerCase() === property.toLowerCase()) {
                            sliderHolder.all(by.xpath('.//div[translate(@title, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz") = "' + property.toLowerCase() + '"]')).first().click();
                            browser.wait(function() {
                                return sliderHolder.all(by.css('.input-txt')).first().isDisplayed();
                            }, 'Waiting for slider input text field.');
                            sliderHolder.all(by.css('.input-txt')).first().clear();
                            sliderHolder.all(by.css('.input-txt')).first().sendKeys(value);
                            return true;
                        }
                    })
                }
            })
        }).then(function(isFreqSet) {
            if (isFreqSet) {
                defer.fulfill();
            }
        });
        return defer.promise;
    },
    /**
     * Sets the value to the given time constant properties.
     * @param panelHolder the container for the panel in which value has to be set.
     * @param property name of the property to set the value.
     * @param value value to be set to the given property.
     */
    "setTimeConstantProperty": function(panelHolder, property, value) {
        var defer = protractor.promise.defer();
        Object.keys(value).forEach(function(timeconstant, index) {
            panelHolder.all(by.css('.input-field-holder')).reduce(function(isPropSet, fieldHolder) {
                if (isPropSet) {
                    return true;
                }
                return fieldHolder.all(by.css('.field-heading.padd-left-zero')).first().getText().then(function(fieldHeading) {
                    if (fieldHeading.toLowerCase() === timeconstant.toLowerCase()) {
                        fieldHolder.all(by.css('.input-txt')).first().clear();
                        fieldHolder.all(by.css('.input-txt')).first().sendKeys(value[timeconstant]);
                        return true;
                    }
                })
            }).then(function(isPropSet) {
                if ((isPropSet) && (index === Object.keys(value).length - 1)) {
                    defer.fulfill();
                }
            });
        })
        return defer.promise;
    },
    /**
     * Sets the value to the given property in bass compensation panel.
     * @param panelHolder the container for the panel in which value has to be set.
     * @param property name of the property to set the value.
     * @param value value to be set to the given property.
     */
    "setCompensationProperties": function(panelHolder, property, value) {
        var self = this;
        if (property.toLowerCase() === 'corner frequency') {
            self.setSliderValue(panelHolder, 'corner frequency', value).then(function() {})
        } else if (property.toLowerCase() === 'align type') {
            return panelHolder.all(by.css('.nominal-loudness-field-holder')).reduce(function(isPropSet, fieldHolder) {
                if (isPropSet) {
                    return true;
                }
                return fieldHolder.all(by.css('.nominal-loudness-field-titles')).first().getText().then(function(propName) {
                    if (propName.toLowerCase() === 'align type') {
                        return fieldHolder.all(by.css('.nominal-loudness-dropdown')).first().all(by.tagName('option')).reduce(function(isOptionSelected, optionElement) {
                            if (isOptionSelected) {
                                return true;
                            }
                            return optionElement.getText().then(function(option) {
                                if (option.toLowerCase() === value.toLowerCase()) {
                                    optionElement.click();
                                    return true;
                                }
                            })
                        })
                    }
                })

            })
        } else if (property.toLowerCase() === 'align order') {
            return panelHolder.all(by.css('.nominal-loudness-field-holder')).reduce(function(isPropSet, fieldHolder) {
                if (isPropSet) {
                    return true;
                }
                return fieldHolder.all(by.css('.nominal-loudness-field-titles')).first().getText().then(function(propName) {
                    if (propName.toLowerCase() === 'align order') {
                        return fieldHolder.all(by.css('.align-order-radio-holder')).reduce(function(isOptionSelected, optionElement) {
                            if (isOptionSelected) {
                                return true;
                            }
                            return optionElement.all(by.css('.css-label')).first().getText().then(function(option) {
                                if (option.toString() === value.toString()) {
                                    optionElement.all(by.css('.css-label')).first().click();
                                    return true;
                                }
                            })
                        })
                    }
                })

            })
        }
    },
    /**
     * Changes the mode in bass compression panel.
     * @param mode Name of the mode to get changed (basic or advanced.)
     */
    "changeMode": function(mode) {
        element(by.css('.bass-cmpr-tab-top-holder')).all(by.css('.bass-cmpr-tab-btn')).reduce(function(isModeChanged, btnElm) {
            if (isModeChanged) {
                return;
            }
            return btnElm.getText().then(function(btnValue) {
                if (btnValue.toLowerCase() === mode.toLowerCase()) {
                    return btnElm.getAttribute('class').then(function(classVal) {
                        if (classVal.indexOf('active') < 0) {
                            return btnElm.click().then(function() {
                                return element(by.css('.accept-restore-default')).isDisplayed().then(function(displayed) {
                                    if (displayed) {
                                        element(by.css('.accept-restore-default')).click();
                                        browser.wait(function() {
                                            return element(by.css('.accept-restore-default')).waitAbsent();
                                        }, 10000, 'Waiting for yes button to disappear.');
                                        return true;
                                    }
                                })
                            })
                        } else {
                            return true;
                        }
                    })
                }
            })
        })
    },
    /**
     * sets the given value to the property in bass compression panel.
     * @param panelHolder the container for the panel in which value has to be set.
     * @param mode name of the mode(basic or advanced)
     * @param property name of the property to set the value.
     * @param value value to be set to the given property.
     */
    "setCompressionProperties": function(panelHolder, mode, property, value) {
        var self = this;
        if (mode === 'basic') {
            self.setSliderValue(panelHolder, property, value).then(function() {});
        } else if (mode === 'advanced') {
            if (property.toLowerCase() === 'morphing control') {
                self.setSliderValue(panelHolder, 'morphing control', value).then(function() {});
            } else if (property.toLowerCase() === 'timeconstants') {
                Object.keys(value).forEach(function(timeconstant) {
                    self.setTimeConstantProperty(panelHolder, timeconstant, value[timeconstant]).then(function() {})
                })
            }
        }
    },
    /**
     * sets the value to the properties of anticlipper.
     * @param panelHolder the container for the panel in which value has to be set.
     * @param property name of the property to set the value.
     * @param value value to be set to the given property.
     */
    "setClipperProperties": function(panelHolder, property, value) {
        var self = this;
        if (property.toLowerCase() === 'threshold') {
            self.setSliderValue(panelHolder, 'threshold', value).then(function() {})
        } else {
            return panelHolder.all(by.css('.sa-max-level-settings-holder')).first().click().then(function() {
                browser.wait(function() {
                    return panelHolder.all(by.css('.custom-popup-content-holder')).first().isDisplayed();
                }, 20000, 'Waiting for pop up of anti clipper to appear.');
                self.setTimeConstantProperty(panelHolder, property, value).then(function() {
                    panelHolder.all(by.css('.snap-popup-close')).first().click().then(function() {
                        browser.wait(function() {
                            return panelHolder.all(by.css('.custom-popup-content-holder')).first().waitAbsent();
                        }, 20000, 'Waiting for pop up of anti clipper to disappear.');
                    })
                })
            })
        }
    },
    /**
     * turns on smart bass in its page.
     */
    "turnOnSmartAmp": function() {
        browser.wait(function() {
            return element.all(by.css('.tflow-total-panels-container')).first().isDisplayed();
        }, 50000, 'Waiting for the smartAmp page.')
        element(by.css('div[title="Turn On Smart Bass"]')).isDisplayed().then(function(displayed) {
            if (displayed) {
                element(by.css('div[title="Turn On Smart Bass"]')).click();
            }
        })
        browser.wait(function() {
            return element(by.css('div[title="Turn On Smart Bass"]')).waitAbsent();
        }, 20000, 'Waiting to turn on smart bass.');
    },
    /**
     * checks whether the given key is present in the given object or not.
     * @param obj object in which the given has to be found.
     * @param key name of the key to search for its presence in the given object.
     */
    "isKeyPresent": function(obj, key) {
        if (Object.keys(obj).indexOf(key) > -1) {
            return true;
        }
        return false;
    },
    /**
     * applies the given testcase to smart bass.
     * @param testCase testcase object which is to be set to smart bass.
     */
    "applyTestCase": function(testCase) {
        var self = this;
        if (self.isKeyPresent(testCase, 'bassCompensation')) {
            if (!self.isKeyPresent(testCase.bassCompensation, 'switch')) {
                testCase.bassCompensation.switch = 'off';
            }
            if (testCase.bassCompensation.switch === 'on') {
                self.switchOnPanel('bass compensation').then(function(panelHolder) {
                    if (Object.keys(testCase.bassCompensation).length > 1) {
                        Object.keys(testCase.bassCompensation).forEach(function(prop) {
                            if (prop.toLowerCase() !== 'switch') {
                                self.setCompensationProperties(panelHolder, prop, testCase.bassCompensation[prop]);
                            }
                        })
                    }
                }, function() {})
            }
        }
        if (self.isKeyPresent(testCase, 'bassCompression')) {
            if (!self.isKeyPresent(testCase.bassCompression, 'switch')) {
                testCase.bassCompression.switch = 'off';
            }
            if (!self.isKeyPresent(testCase.bassCompression, 'mode')) {
                testCase.bassCompression.mode = 'advanced';
            }
            if (testCase.bassCompression.switch === 'on') {
                self.switchOnPanel('bass compression').then(function(panelHolder) {
                    self.changeMode(testCase.bassCompression.mode);
                    if (Object.keys(testCase.bassCompression).length > 2) {
                        Object.keys(testCase.bassCompression).forEach(function(prop) {
                            if ((prop.toLowerCase() !== 'switch') && (prop.toLowerCase() !== 'mode')) {
                                self.setCompressionProperties(panelHolder, testCase.bassCompression.mode, prop, testCase.bassCompression[prop]);
                            }
                        })
                    }
                }, function() {})
            }
        }
        if (self.isKeyPresent(testCase, 'antiClipper')) {
            if (!self.isKeyPresent(testCase.antiClipper, 'switch')) {
                testCase.antiClipper.switch = 'off';
            }
            if (testCase.antiClipper.switch === 'on') {
                self.switchOnPanel('anti clipper').then(function(panelHolder) {
                    if (Object.keys(testCase.antiClipper).length > 1) {
                        Object.keys(testCase.antiClipper).forEach(function(prop) {
                            if (prop.toLowerCase() !== 'switch') {
                                self.setClipperProperties(panelHolder, prop, testCase.antiClipper[prop]);
                            }
                        })
                    }
                }, function() {})
            }
        }
    }
}
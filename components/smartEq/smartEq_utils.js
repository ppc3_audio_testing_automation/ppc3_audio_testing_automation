var crossover = require('../crossover/crossover_utils.js');
var waitAbsent = require('../lib/waitAbsent.js');
var fs = require('fs');
module.exports = {
    /**
     * Sets the smoothing factor.
     * @param value value to be set as smoothing factor.
     * Resolves when the value is set.
     * Rejects if the value is not present as an option in the drop down.
     */
    "setSmoothingFactor": function(value) {
        var defer = protractor.promise.defer();
        element(by.css('.smarteq-original-controls-holder')).all(by.id('sfactor-seq')).first().all(by.tagName('option')).reduce(function(optionIndex, optionHolder, index) {
            if (optionIndex > -1) {
                return optionIndex;
            }
            return optionHolder.getText().then(function(option) {
                if (option.toString() === value.toString()) {
                    optionHolder.click();
                    return index;
                }
            })
        }).then(function(optionIndex) {
            if (optionIndex === undefined) {
                defer.reject(value + ' not found as an option in smoothing factor.');
            } else {
                defer.fulfill();
            }
        })
        return defer.promise;
    },
    /**
     * Sets the dip supression.
     * @param value value to be set as dip supression.
     */
    "setDipSupression": function(value) {
        element(by.css('.smarteq-original-controls-holder')).all(by.css('label[for="dip-suppression"]')).first().click().then(function() {
            element(by.css('.smarteq-original-controls-holder')).all(by.css('.smarteq-inner-sep-holder.ds')).first().all(by.css('.slider-value-holder')).first().click();
            browser.wait(function() {
                return element(by.css('.smarteq-original-controls-holder')).all(by.css('input[dip-suppression-validator]')).first().isDisplayed();
            }, 6000, 'Waiting for dip suppression text field');
            element(by.css('.smarteq-original-controls-holder')).all(by.css('input[dip-suppression-validator]')).first().clear();
            element(by.css('.smarteq-original-controls-holder')).all(by.css('input[dip-suppression-validator]')).first().sendKeys(value);
        })

    },
    /**
     * sets the spline region.
     * @param value a string in format of <sr1>-<sr2> to be set as spline regiom range.
     * resolves when the value is set.
     * rejects if the value is not in the required format.
     */
    "setSplineRegion": function(value) {
        var defer = protractor.promise.defer();
        var range = value.split('-');
        if (range.length !== 2) {
            defer.reject('Spline Range:' + value + ' not specified correctly. The format for spline region is <sr1>-<sr2>');
        } else {
            var sr1 = range[0];
            var sr2 = range[1];
            browser.wait(function() {
                return element.all(by.css('.smarteq-original-controls-holder')).first().all(by.css('.loglinear-holder')).get(1).isDisplayed();
            }, 2000, 'Waiting for manual spline.');
            element.all(by.css('.smarteq-original-controls-holder')).first().all(by.css('.loglinear-holder')).get(1).click();
            element(by.css('.smarteq-original-controls-holder')).all(by.css('p[ng-click="hideSlider()"]')).first().click();
            element(by.css('.smarteq-original-controls-holder')).all(by.id('srsmarteqoff1')).first().clear();
            element(by.css('.smarteq-original-controls-holder')).all(by.id('srsmarteqoff1')).first().sendKeys(sr1);
            element(by.css('.smarteq-original-controls-holder')).all(by.id('srsmarteqoff2')).first().clear();
            element(by.css('.smarteq-original-controls-holder')).all(by.id('srsmarteqoff2')).first().sendKeys(sr2).then(function() {
                defer.fulfill();
            })
        }
        return defer.promise;
    },
    /**
     * sets spl offset
     * @param value value to be set as spl offset.
     */
    "setSplOffset": function(value) {
        browser.wait(function() {
            return element.all(by.css('.smarteq-original-controls-holder')).first().all(by.css('.slider-value-holder')).first().isDisplayed();
        }, 2000, 'Waiting for spl off set slider value holder.');
        element.all(by.css('.smarteq-original-controls-holder')).first().all(by.css('.slider-value-holder')).first().click();
        browser.wait(function() {
            return element.all(by.css('.smarteq-original-controls-holder')).first().all(by.css('.input-txt.spl')).first().isDisplayed();
        }, 2000, 'Waiting for spl offset text field.');
        element.all(by.css('.smarteq-original-controls-holder')).first().all(by.css('.input-txt.spl')).first().clear();
        element.all(by.css('.smarteq-original-controls-holder')).first().all(by.css('.input-txt.spl')).first().sendKeys(value);
    },
    /**
     * sets the type of preset target curve.
     * @param type type pf target curve.
     * resolves when the type is selected.
     * rejects when type is not present in the UI.
     */
    "setPresetTargetCurve": function(type) {
        var defer = protractor.promise.defer();
        element.all(by.repeater('preset in appData.smartEQObj.props.targetCurvePresets.options')).reduce(function(optionIndex, typeHolder, index) {
            if (optionIndex > -1) {
                return optionIndex;
            }
            return typeHolder.all(by.css('span[ng-mouseenter="showPresetTooltip($index)"]')).first().getText().then(function(option) {
                if (option.toLowerCase() === type.toLowerCase()) {
                    typeHolder.all(by.tagName('label')).first().click();
                    return index;
                }
            })
        }).then(function(optionIndex) {
            element.all(by.css('.smarteq-tile-sec-holder.controls.no-brdr-btm')).first().click();
            if (optionIndex === undefined) {
                defer.reject(type + ' type of target curve is not found.');
            } else {
                defer.fulfill();
            }
        });
        return defer.promise;
    },
    /**
     * sets the number of biquads.
     * @param value number of biquads to be used.
     * resolves when the number of biquads are selected.
     * rejects when the specified value is not present as an option in the dropdown.
     */
    "setNumberOfBiquads": function(value) {
        var defer = protractor.promise.defer();
        element(by.id('noof-biquads-seq')).all(by.tagName('option')).reduce(function(optionIndex, optionHolder, index) {
            if (optionIndex > -1) {
                return optionIndex;
            }
            return optionHolder.getText().then(function(option) {
                if (option.toString() === value.toString()) {
                    optionHolder.click();
                    return index;
                }
            })
        }).then(function(optionIndex) {
            if (optionIndex === undefined) {
                defer.reject(value + ' not found as an option for number of biquads.');
            } else {
                defer.fulfill();
            }
        })
        return defer.promise;
    },
    /**
     * sets the type of algorithm.
     * @param value name of algorithm.
     * resolves when the algorithm is selected.
     * rejects when the algorithm is not present as an option in the UI.
     */
    "setAlgorithm": function(value) {
        var defer = protractor.promise.defer();
        element.all(by.repeater('algo in appData.smartEQObj.props.algoFunc.options')).reduce(function(optionIndex, typeHolder, index) {
            if (optionIndex > -1) {
                return optionIndex;
            }
            return typeHolder.all(by.tagName('span')).first().getText().then(function(option) {
                if (option.toLowerCase() === value.toLowerCase()) {
                    typeHolder.all(by.tagName('label')).first().click();
                    return index;
                }
            })
        }).then(function(optionIndex) {
            if (optionIndex === undefined) {
                defer.reject(value + ' type of algorithm is not found.');
            } else {
                defer.fulfill();
            }
        })
        return defer.promise;
    },
    /**
     * sets the parametric maximum frequency.
     * @param value value to be set as the max frequency.
     */
    "setParametricMaxFreq": function(value) {
        element(by.id('smarteq-panel-settings3')).click();
        browser.wait(function() {
            return element(by.css('div[click-outside-directive="closeSettingDropdown(2)"]')).isDisplayed();
        }, 2000, 'Waiting for paramtric max frequncy settings drop down.');
        element(by.model('appData.smartEQObj.props.PEQMaxFreqForFit.value')).clear();
        element(by.model('appData.smartEQObj.props.PEQMaxFreqForFit.value')).sendKeys(value);
    },
    /**
     * uploads the spl data mentioned in the test case.
     * resolves when spl data is loaded.
     * rejects when spl data is not mentioned in the test case or when loadSplData() rejects.
     */
    "uploadSplData": function(testCase) {
        var self = this;
        var defer = protractor.promise.defer();
        self.isKeyPresent(testCase, 'splData').then(function() {
            Object.keys(testCase.splData).forEach(function(type, index) {
                self.loadSplData(type, testCase.splData[type]).then(function() {
                    if (index === Object.keys(testCase.splData).length - 1) {
                        defer.fulfill();
                    }
                }, function(reason) {
                    defer.reject(reason);
                })
            })
        }, function() {
            defer.reject('SPL data not present.Testcase does not have a field named splData.');
        })
        return defer.promise;
    },
    /**
     * loads the spl data.
     * @param type type of speaker('left / gang' or 'right').
     * @param dataPath path to spl data.
     * resolves when the data is loaded.
     * rejects when file does not exist or speaker type is not mentioned correctly.
     */
    "loadSplData": function(type, dataPath) {
        var defer = protractor.promise.defer();
        if (fs.existsSync(dataPath)) {
            element(by.model('appData.splUI.selectedSplid')).all(by.tagName('option')).reduce(function(optionIndex, optionHolder, index) {
                if (optionIndex > -1) {
                    return optionIndex;
                }
                return optionHolder.getText().then(function(option) {
                    if (option.toLowerCase().trim() === type.toLowerCase().trim()) {
                        optionHolder.click();
                        return index;
                    }
                })
            }).then(function(optionIndex) {
                if (optionIndex !== undefined) {
                    crossover.uploadSplData(dataPath).then(function() {
                        defer.fulfill();
                    }, function(reason) {
                        defer.reject(reason);
                    })
                } else {
                    defer.reject('option:' + type + ' not present for type of spl data.')
                }
            })
        } else {
            defer.reject('SpldataPath:' + dataPath + ' does not exist.');
        }

        return defer.promise;
    },
    /**
     * checks if key is present in the given object.
     * @param obj object to check for the presence of key.
     * @param key key to check its existence in the given object.
     * resolves when key is present in the object.
     * rejects if key is not present in the object.
     */
    "isKeyPresent": function(obj, key) {
        var defer = protractor.promise.defer();
        if (Object.keys(obj).indexOf(key) > -1) {
            defer.fulfill();
        } else {
            defer.reject();
        }
        return defer.promise;
    },
    /**
     * sets advanced clean up settings(spl offset, spline region).
     * @param testCase test case object.
     * resolves when the values are not. 
     * rejects when setSplineRegion() rejects.
     */
    "setAdvancedCleanUpSettings": function(testCase) {
        var self = this;
        var defer = protractor.promise.defer();
        browser.wait(function() {
            return element(by.css('.smarteq-original-controls-holder')).isDisplayed();
        }, 20000, 'Waiting for original smarteq content holder.');
        element.all(by.css('.smarteq-panel-settings.spl-adv-appwalkthrough')).first().click().then(function() {
            if (self.isKeyPresent(testCase, 'splOffset')) {
                self.setSplOffset(testCase.splOffset);
            }
            if (self.isKeyPresent(testCase, 'splineRegion')) {
                self.setSplineRegion(testCase.splineRegion).then(function() {
                    element.all(by.css('.smarteq-tile-sec-holder.controls.no-brdr-btm')).first().click();
                    defer.fulfill();
                }, function(reason) {
                    defer.reject(reason);
                })
            } else {
                element.all(by.css('.smarteq-tile-sec-holder.controls.no-brdr-btm')).first().click();
                defer.fulfill();
            }
        });
        return defer.promise;
    },
    /**
     * applies the test case.
     * @param testCase test case object.
     * resolves when test case is applied and filters are computed.
     * rejects when any of the fields fail to update or when the smartEq is not able to compute the filters.
     */
    "applyTestCase": function(testCase) {
        var self = this;
        var defer = protractor.promise.defer();
        element(by.id('use-smarteq-btn')).click().then(function() {
            browser.wait(function() {
                return element.all(by.css('.smarteq-original-controls-holder')).first().isDisplayed();
            }, 20000, 'Waiting for original controls holder.');
            self.isKeyPresent(testCase, 'smoothingFactor').then(function() {
                self.setSmoothingFactor(testCase.smoothingFactor).then(function() {}, function(reason) {
                    defer.reject(reason);
                })
            }, function() {}).then(function() {
                self.isKeyPresent(testCase, 'dipSuppression').then(function() {
                    self.setDipSupression(testCase.dipSuppression);
                }, function() {})
            }).then(function() {
                self.isKeyPresent(testCase, 'splOffset').then(function() {
                    self.setAdvancedCleanUpSettings(testCase).then(function() {}, function(reason) {
                        defer.reject(reason);
                    })
                }, function() {
                    self.isKeyPresent(testCase, 'splineRegion').then(function() {
                        self.setAdvancedCleanUpSettings(testCase).then(function() {}, function(reason) {
                            defer.reject(reason);
                        })
                    }, function() {})
                })
            }).then(function() {
                self.isKeyPresent(testCase, 'presetTargetCurve').then(function() {
                    self.setPresetTargetCurve(testCase.presetTargetCurve).then(function() {}, function(reason) {
                        defer.reject(reason);
                    })
                }, function() {})
            }).then(function() {
                self.isKeyPresent(testCase, 'noOfBiquads').then(function() {
                    self.setNumberOfBiquads(testCase.noOfBiquads).then(function() {}, function(reason) {
                        defer.reject(reason);
                    })
                }, function() {})
            }).then(function() {
                self.isKeyPresent(testCase, 'parametricMaxFreq').then(function() {
                    self.setParametricMaxFreq(testCase.parametricMaxFreq);
                }, function() {})
            }).then(function() {
                self.isKeyPresent(testCase, 'algorithm').then(function() {
                    self.setAlgorithm(testCase.algorithm).then(function() {}, function(reason) {
                        defer.reject(reason);
                    })
                }, function() {});
            })
        }).then(function() {
            element(by.id('run-smarteq')).click().then(function() {
                browser.wait(function() {
                    return element.all(by.css('.overlay-txt-processing')).first().isDisplayed();
                }, 50000, 'Waiting for the smarteq processing overlay.');
                browser.wait(function() {
                    return element.all(by.css('.overlay-txt-processing')).first().waitAbsent();
                }, 50000, 'Waiting for the smarteq processing overlay to disappear.');
                element.all(by.css('.biquad_graph_container.orig')).first().all(by.css('.error-txt')).first().isDisplayed().then(function(displayed) {
                    if (displayed) {
                        element.all(by.css('.biquad_graph_container.orig')).first().all(by.css('.error-txt')).first().getText().then(function(errorMsg) {
                            if (errorMsg !== 'Updating the curves...') {
                                defer.reject(errorMsg);
                            } else {
                                defer.fulfill();
                            }
                        })
                    } else {
                        defer.fulfill();
                    }
                })
            })
        });
        return defer.promise;
    }
}
var smartEq = require('./smartEq_utils.js');
var commonUtils = require('../commonUtils.js');
var smartEq = require('../smartEq/smartEq_utils.js');
var chai = require('chai');
var expect = chai.expect;
var path = require('path');
module.exports = {
    /**
     * Tests the smartEq.
     * @param rootPath Current working directory path.
     * @param runInput Contents of config.json file.
     * @param audioPlayer audio player component to call its functions.
     * @param currentConfig Name of the configurations which is currently opened.
     * @param currentAudioMode Name of the audio mode which is currently opened.
     */
    test: function(rootPath, runInput, audioPlayer, currentConfig, currentAudioMode) {
        var defer = protractor.promise.defer();
        var isComponentPresent = false;
        var isNotCombined = false,
            testCaseApplied = false,
            errorMsg, isSpluploaded = false;
        var testInputs = require('./smartEq_testcases.json');
        testCases = commonUtils.findTestCasesToRun(testInputs.testCases, runInput, "smartEq");
        testCases.forEach(function(testCase, index) {
            it("should write test case " + testCase.id, function() {
                commonUtils.isSpeakerNotCombined("SmartEQ").then(function() {
                    isNotCombined = true;
                }, function() {});
                commonUtils.switchOnComponent('SmartEQ').then(function(componentIndex) {
                    isComponentPresent = true;
                    smartEq.uploadSplData(testCase).then(function() {
                        isSpluploaded = true;
                        browser.wait(function() {
                            return element(by.css('.nw-import-panel-container.spl-pup-outer-container')).waitAbsent();
                        }, 50000, 'Waiting for spl pop up to disappear.');
                        commonUtils.openComponent(componentIndex);
                    }, function(reason) {
                        isSpluploaded = false;
                        errorMsg = reason;
                    });
                }, function() {}).then(function() {
                    expect(isComponentPresent, 'smartEq not present').to.be.true;
                    expect(isSpluploaded, 'SPL data not uploaded.' + errorMsg).to.be.true;
                    if (isComponentPresent && isSpluploaded) {
                        smartEq.applyTestCase(testCase).then(function() {
                            testCaseApplied = true;
                            commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                                var deviceType;
                                if (isNotCombined) {
                                    deviceType = 'Woofer_A';
                                } else {
                                    deviceType = 'A';
                                }
                                commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, deviceType, runInput.appToTest);
                            });
                            commonUtils.applySnapshot().then(function() {});
                            commonUtils.goBackToAudioProcessingPage();

                            commonUtils.isDeviceBAvailable().then(function() {
                                commonUtils.switchDevice('B');
                                commonUtils.switchOnComponent("SmartEQ").then(function(index) {
                                    smartEq.uploadSplData(testCase).then(function() {
                                        browser.wait(function() {
                                            return element(by.css('.nw-import-panel-container.spl-pup-outer-container')).waitAbsent();
                                        }, 50000, 'Waiting for spl pop up to disappear.');
                                        commonUtils.openComponent(index);
                                    }, function() {});
                                }, function() {}).then(function() {
                                    smartEq.applyTestCase(testCase).then(function() {
                                        commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                                            commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'B', runInput.appToTest);
                                        });
                                    }, function() {});
                                    commonUtils.applySnapshot();
                                    commonUtils.goBackToAudioProcessingPage();
                                    commonUtils.switchDevice('A');
                                }, function(error) {})
                            }, function() {}).then(function() {
                                if (isNotCombined) {
                                    commonUtils.switchToSpeaker('Tweeter');
                                    commonUtils.switchOnComponent("smartEQ").then(function(index) {
                                        commonUtils.openComponent(index);
                                        smartEq.applyTestCase(testCase).then(function() {
                                            commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                                                commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'Tweeter_A', runInput.appToTest);
                                            });
                                        }, function() {});
                                        commonUtils.applySnapshot();
                                        commonUtils.goBackToAudioProcessingPage();
                                        commonUtils.switchToSpeaker('Woofer');
                                    }, function() {})
                                }
                            })
                        }, function(reason) {
                            errorMsg = reason;
                            commonUtils.applySnapshot();
                            commonUtils.goBackToAudioProcessingPage();
                            testCaseApplied = false;
                            if (index === testCases.length - 1) {
                                defer.fulfill();
                            }
                        }).then(function() {
                            if (index === testCases.length - 1) {
                                defer.fulfill();
                            }
                            expect(testCaseApplied, 'Test case not applied due to: ' + errorMsg).to.be.true;
                        });
                    }
                })
            })
        });
        return defer.promise;
    }
}
var fs = require('fs');
var path = require('path');
var commonUtils = require('../commonUtils.js');
var crossover = require('./crossover_utils.js');
var chai = require('chai');
var expect = chai.expect;

module.exports = {
    /**
     * Tests the crossover.
     * @param rootPath Current working directory path.
     * @param runInput Contents of config.json file.
     * @param audioPlayer audio player component to call its functions.
     * @param currentConfig Name of the configurations which is currently opened.
     * @param currentAudioMode Name of the audio mode which is currently opened.
     */

    test: function(rootPath, runInput, audioPlayer, currentConfig, currentAudioMode) {
        var defer = protractor.promise.defer();
        var isComponentPresent = false;
        var isNotCombined = false,
            componentIndex, isSplLoaded = false,
            errorMsg, testCaseApplied;
        var testInputs = require('./crossover_testcases.json');
        testCases = commonUtils.findTestCasesToRun(testInputs.testCases, runInput, "crossover");
        testCases.forEach(function(testCase, index) {
            it("should write test case " + testCase.id, function() {

                commonUtils.isSpeakerNotCombined("Crossover").then(function() {
                    isNotCombined = true;
                }, function() {});
                commonUtils.switchOnComponent("Crossover").then(function(index) {
                    isComponentPresent = true;
                    crossover.loadSplData(rootPath, testCase).then(function() {
                        isSplLoaded = true;
                        commonUtils.openComponent(index);
                    }, function(reason) {
                        isSplLoaded = false;
                        errorMsg = reason;
                    })
                }, function() {}).then(function() {
                    expect(isComponentPresent, 'Crossover not present').to.be.true;
                    expect(isSplLoaded, 'Spl Data not loaded.' + errorMsg).to.be.true;
                    if (isComponentPresent && isSplLoaded) {
                        crossover.applyTestCase(testCase).then(function() {
                            testCaseApplied = true;
                            commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                                var deviceType;
                                if (isNotCombined) {
                                    deviceType = 'Woofer_A';
                                } else {
                                    deviceType = 'A';
                                }
                                commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, deviceType, runInput.appToTest);
                            });
                            commonUtils.applySnapshot().then(function() {});
                            commonUtils.goBackToAudioProcessingPage();

                            commonUtils.isDeviceBAvailable().then(function() {
                                commonUtils.switchDevice('B');
                                commonUtils.switchOnComponent("Crossover").then(function(index) {
                                    crossover.loadSplData(rootPath, testCase).then(function() {
                                        commonUtils.openComponent(index);
                                    }, function() {});
                                }, function() {}).then(function() {
                                    crossover.applyTestCase(testCase).then(function() {
                                        commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                                            commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'B', runInput.appToTest);
                                        });
                                    }, function() {});
                                    commonUtils.applySnapshot();
                                    commonUtils.goBackToAudioProcessingPage();
                                    commonUtils.switchDevice('A');
                                }, function(error) {})
                            }, function() {}).then(function() {
                                if (isNotCombined) {
                                    commonUtils.switchToSpeaker('Tweeter');
                                    commonUtils.switchOnComponent("Crossover").then(function(index) {
                                        commonUtils.openComponent(index);
                                        crossover.applyTestCase(testCase).then(function() {
                                            commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                                                commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'Tweeter_A', runInput.appToTest);
                                            });
                                        }, function() {});
                                        commonUtils.applySnapshot();
                                        commonUtils.goBackToAudioProcessingPage();
                                        commonUtils.switchToSpeaker('Woofer');
                                    }, function() {})
                                }
                            })
                        }, function(reason) {
                            errorMsg = reason;
                            commonUtils.applySnapshot();
                            commonUtils.goBackToAudioProcessingPage();
                            testCaseApplied = false;
                            if (index === testCases.length - 1) {
                                defer.fulfill();
                            }
                        }).then(function() {
                            if (index === testCases.length - 1) {
                                defer.fulfill();
                            }
                            expect(testCaseApplied, 'Test case not applied due to: ' + errorMsg).to.be.true;
                        });
                    }
                })
            })
        })
        return defer.promise;
    }
}
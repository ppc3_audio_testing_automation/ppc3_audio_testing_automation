var equalizer = require('../equalizer/equalizer_Utils.js');
var waitAbsent = require('../lib/waitAbsent.js');
var commonUtils = require('../commonUtils.js');
var path = require('path');
var fs = require('fs');
module.exports = {
    /**
     * sets the biquad properties.
     * @param biquadId biquad number(1,2,3,..).
     * @param speaker type of speaker(woofer or tweeter).
     * @param testCase an object conatining biquad details.
     * Resolves when properties are set.
     * Rejects when the specified biquad is not present.
     */
    "setBiquadValue": function(biquadId, speaker, testCase) {
        var defer = protractor.promise.defer();
        var self = this;
        var speakerContainer;
        if (speaker === 'woofer') {
            speakerContainer = element(by.css('.crp-bottom-left-container'));
        } else if (speaker === 'tweeter') {
            speakerContainer = element(by.css('.crp-bottom-right-container'));
        }
        if (speakerContainer !== undefined) {
            speakerContainer.all(by.css('.tabular_view_innersection.front')).count().then(function(biquadCount) {
                if (biquadId <= biquadCount) {
                    var biquadHolder = speakerContainer.all(by.css('.tabular_view_innersection.front')).get(biquadId - 1);
                    equalizer.switchOnBiquad(biquadHolder).then(function() {
                        self.isKeyPresent(testCase.biquads[biquadId], 'filterType').then(function() {
                            equalizer.applyFilterType(biquadHolder, testCase.biquads, biquadId, 'biquad.props.filter').then(function() {});
                        }, function() {}).then(function() {
                            if (Object.keys(testCase.biquads[biquadId]).length > 1) {
                                equalizer.applyKeyInputs(biquadHolder, testCase.biquads, biquadId);
                            }
                            defer.fulfill();
                        })
                    })
                } else {
                    defer.reject('biquad:' + biquadId + ' not present.');
                }
            });
        }
        return defer.promise;
    },
    /**
     * ungangs the tweeter in advanced options.
     * @param speakerHolder container of the speaker in advanced settings.
     */
    "unGangTweeter": function(speakerHolder) {
        speakerHolder.all(by.css('label[for="tweeterDelay"]')).first().isPresent().then(function(present) {
            if (present) {
                speakerHolder.all(by.css('label[for="tweeterDelay"]')).first().isDisplayed().then(function(displayed) {
                    if (displayed) {
                        speakerHolder.all(by.id('tweeterDelay')).first().getAttribute('checked').then(function(isChecked) {
                            if (isChecked) {
                                speakerHolder.all(by.css('label[for="tweeterDelay"]')).first().click();
                            }
                        })
                    }
                })
            }
        })
    },
    /**
     * sets the delay.
     * @param speakerHolder container of speaker in advanced settings.
     * @param delayType type of delay(delay or delay(L) or delay(R))
     * @param value value to be set as delay.
     * Resolves when delay is set.
     */
    "setDelay": function(speakerHolder, delayType, value) {
        var defer = protractor.promise.defer();
        var self = this;
        speakerHolder.all(by.css('.input-field-holder')).reduce(function(isDelaySet, inputHolder) {
            if (isDelaySet) {
                return;
            }
            return inputHolder.all(by.css('.biquad-ddown-heading.width-85px.float-left.text-align-right')).first().isPresent().then(function(present) {
                if (present) {
                    return inputHolder.all(by.css('.biquad-ddown-heading.width-85px.float-left.text-align-right')).first().getText().then(function(delayHeading) {
                        if (delayHeading.toLowerCase() === delayType.toLowerCase()) {
                            inputHolder.all(by.css('.cro-ddown-input')).first().clear();
                            inputHolder.all(by.css('.cro-ddown-input')).first().sendKeys(value);
                            return true;
                        }
                    })
                }
            })
        }).then(function() {
            defer.fulfill();
        })
        return defer.promise;
    },
    /**
     * uploads spl data.
     * @param splData path to spl data.
     * Resolves when spl data is loaded.
     * Rejects when spl data path does not exists or when the spl data is invalid.
     */
    "uploadSplData": function(splData) {
        var defer = protractor.promise.defer();
        if (!fs.existsSync(splData)) {
            defer.reject('splData:' + splData + ' does not exist.');
        } else {
            element(by.css('input[title="Click to load SPL data file"]')).isPresent().then(function(displayed) {
                if (displayed) {
                    element(by.css('input[title="Click to load SPL data file"]')).sendKeys(splData).then(function() {
                        element(by.css('div[title="App status will be shown here"]')).isPresent().then(function(displayed) {
                            if (displayed) {
                                defer.reject('Invalid spl data:' + splData);
                            } else {
                                browser.wait(function() {
                                    return element(by.css('.nw-import-panel-container.spl-pup-outer-container')).isDisplayed();
                                }, 50000, 'Waiting for spl pop up');
                                element(by.css('.spl-btm-btn')).click().then(function() {
                                    defer.fulfill();
                                })
                            }
                        })
                    })
                }
            })
        }
        return defer.promise;
    },
    /**
     * Loads spl data.
     * @param rootPath path of current working directory.
     * @param testCase test case object.
     * resolves when data is loaded.
     * rejects when uploadSplData() rejects.
     */
    "loadSplData": function(rootPath, testCase) {
        var self = this;
        var defer = protractor.promise.defer();
        self.isKeyPresent(testCase, 'wooferSplData').then(function() {}, function() {
            testCase.wooferSplData = rootPath + '/resources/splFiles/woofer spl.txt';
        }).then(function() {
            self.isKeyPresent(testCase, 'tweeterSplData').then(function() {}, function() {
                testCase.tweeterSplData = rootPath + '/resources/splFiles/tweeter spl.txt';
            })
        }).then(function() {
            self.uploadSplData(path.normalize(testCase.wooferSplData)).then(function() {
                browser.wait(function() {
                    return element(by.css('.nw-import-panel-container.spl-pup-outer-container')).waitAbsent();
                }, 50000, 'Waiting for spl pop up to disappear.');
                commonUtils.switchToSpeaker('Tweeter');
                self.uploadSplData(path.normalize(testCase.tweeterSplData)).then(function() {
                    browser.wait(function() {
                        return element(by.css('.nw-import-panel-container.spl-pup-outer-container')).waitAbsent();
                    }, 50000, 'Waiting for spl pop up to disappear.');
                    commonUtils.switchToSpeaker('Woofer');
                    defer.fulfill();
                }, function(reason) {
                    defer.reject(reason);
                })
            }, function(reason) {
                defer.reject(reason)
            });
        })
        return defer.promise;
    },
    /**
     * sets the properties of advanced settings.
     * @param setting advancedSettings object of test case.
     * resolves when fields are set.
     * rejects when smooth factor does not have the specified option or when spline region does not have the required(<sr1>-<sr2>) format.
     */
    "setAdvancedSettings": function(setting) {
        var self = this;
        var defer = protractor.promise.defer();
        element(by.id('crp-settings-control-trigger')).click().then(function() {
            browser.wait(function() {
                return element.all(by.css('.nw-drc-top-settings-control-holder')).first().isDisplayed();
            }, 50000, 'Waiting for crossover adavnced control.');
            self.isKeyPresent(setting, 'smoothFactor').then(function() {
                element(by.model('appData.wooferData.splInstance.props.smoothFactor.value')).all(by.tagName('option')).reduce(function(isOptionSet, optionHolder) {
                    if (isOptionSet) {
                        return isOptionSet;
                    }
                    return optionHolder.getText().then(function(option) {
                        if (option.toString() === setting.smoothFactor.toString()) {
                            optionHolder.click();
                            return true;
                        }
                    })
                }).then(function(isOptionSet) {
                    if (isOptionSet !== true) {
                        defer.reject('Smooth factor:' + setting.smoothFactor + ' not found as an option.');
                    }
                })
            }, function() {}).then(function() {
                self.isKeyPresent(setting, 'splOffset').then(function() {
                    element(by.css('div[title="SPL Offset"]')).click();
                    browser.wait(function() {
                        return element.all(by.css('.nw-drc-top-settings-control-holder')).first().all(by.css('input[ng-change="setOldVal(0);change()"]')).first().isDisplayed();
                    }, 2000, 'Waiting for spl offset text field');
                    element.all(by.css('.nw-drc-top-settings-control-holder')).first().all(by.css('input[ng-change="setOldVal(0);change()"]')).first().clear();
                    element.all(by.css('.nw-drc-top-settings-control-holder')).first().all(by.css('input[ng-change="setOldVal(0);change()"]')).first().sendKeys(setting.splOffset);
                }, function() {})
            }).then(function() {
                self.isKeyPresent(setting, 'splineRegion').then(function() {
                    var range = setting.splineRegion.split('-');
                    element(by.id('maual-spline')).isPresent().then(function(present) {
                        if (present) {
                            element(by.id('maual-spline')).isDisplayed().then(function(displayed) {
                                if (displayed) {
                                    if (range.length !== 2) {
                                        defer.reject('spline range is not specified correctly.It must be in format of <sr1>-<sr2>.');
                                    } else {
                                        var sr1 = range[0];
                                        var sr2 = range[1];
                                        element(by.id('maual-spline')).click();
                                        element.all(by.css('p[ng-click="hideSlider()"]')).first().click();
                                        browser.wait(function() {
                                            return element(by.id('sr1')).isDisplayed();
                                        }, 2000, 'Waiting for spline region text field');
                                        element(by.id('sr1')).clear();
                                        element(by.id('sr1')).sendKeys(sr1);
                                        element(by.id('sr2')).clear();
                                        element(by.id('sr2')).sendKeys(sr2);
                                    }
                                }
                            })
                        }
                    })
                }, function() {});
            }).then(function() {
                Object.keys(setting).forEach(function(settingProperty, index) {
                    if ((settingProperty === 'woofer') || (settingProperty === 'tweeter')) {
                        element.all(by.css('.nw-drc-top-settings-control-holder')).first().all(by.css('.crossover-ddown-section')).reduce(function(isPropset, propHolder) {
                            if (isPropset) {
                                return;
                            }
                            return propHolder.all(by.css('.biquad-ddown-heading.float-left')).first().getText().then(function(propName) {
                                if (propName.toLowerCase() === settingProperty.toLowerCase()) {
                                    Object.keys(setting[settingProperty]).forEach(function(speakerProp) {
                                        if (speakerProp === 'invertPhase') {
                                            if (setting[settingProperty].invertPhase === "true") {
                                                return propHolder.all(by.css('label[for="invertPhase-' + settingProperty.toLowerCase() + '"]')).first().click().then(function() {})
                                            }
                                            return true;
                                        } else if (speakerProp === 'gang') {
                                            if ((settingProperty === 'tweeter') && !setting.tweeter.gang) {
                                                self.unGangTweeter(propHolder);
                                            }
                                        } else {
                                            if (settingProperty === 'woofer') {
                                                return self.setDelay(propHolder, speakerProp, setting[settingProperty][speakerProp]).then(function() {
                                                    return true;
                                                })
                                            } else if (settingProperty === 'tweeter') {
                                                return self.isKeyPresent(setting[settingProperty], 'gang').then(function() {}, function() {
                                                    setting.tweeter.gang = true;
                                                }).then(function() {
                                                    if (setting.tweeter.gang) {
                                                        return self.isKeyPresent(setting.tweeter, 'delay').then(function() {
                                                            return self.setDelay(propHolder, 'delay', setting.tweeter.delay).then(function() {
                                                                return true;
                                                            })
                                                        }, function() {
                                                            return true;
                                                        })
                                                    } else if (!setting.tweeter.gang) {
                                                        return self.setDelay(propHolder, speakerProp, setting[settingProperty][speakerProp]).then(function() {
                                                            return true;
                                                        })
                                                    }
                                                })
                                            }
                                        }
                                    });
                                }
                            })
                        })
                    }
                })
            }).then(function() {
                defer.fulfill();
            })
        });
        return defer.promise;
    },
    /**
     * checks whether the given key is present in the given object.
     * @param obj object to check for the presence of key.
     * @param key key to check its presence in the object.
     * resolves when the key is present in the object.
     * rejects when the key is not present in the object.
     */
    "isKeyPresent": function(obj, key) {
        var defer = protractor.promise.defer();
        if (Object.keys(obj).indexOf(key) > -1) {
            defer.fulfill();
        } else {
            defer.reject();
        }
        return defer.promise;
    },
    /**
     * switches off the biquad.
     * @param biquadId biquad number.
     * @param speaker name of speaker type(woofer or tweeter).
     * resolves when the given biquad is switched off.
     */
    "switchOffBiquad": function(biquadId, speaker) {
        var defer = protractor.promise.defer();
        var speakerContainer;
        if (speaker === 'woofer') {
            speakerContainer = element(by.css('.crp-bottom-left-container'));
        } else if (speaker === 'tweeter') {
            speakerContainer = element(by.css('.crp-bottom-right-container'));
        }
        if (speakerContainer !== undefined) {
            speakerContainer.all(by.css('.tabular_view_innersection.front')).count().then(function(biquadCount) {
                if (biquadId <= biquadCount) {
                    var current_biquad = speakerContainer.all(by.css('.tabular_view_innersection.front')).get(biquadId - 1);
                    current_biquad.all(by.css('.on-switch')).first().isDisplayed().then(function(displayed) {
                        if (displayed) {
                            current_biquad.all(by.css('.on-switch')).first().click();
                        }
                        defer.fulfill();
                    })
                }
            })
        }
        return defer.promise;
    },
    /**
     * applies the test case.
     * @param testCase test case object.
     * resolves when the test case is applied.
     * rejects when setAdvancedSettings() or setBiquadValue() function rejects.
     */
    "applyTestCase": function(testCase) {
        var self = this;
        var defer = protractor.promise.defer();
        self.isKeyPresent(testCase, 'advancedSettings').then(function() {
            self.setAdvancedSettings(testCase.advancedSettings).then(function() {}, function(reason) {
                defer.reject(reason);
            })
        }, function() {}).then(function() {
            Object.keys(testCase).forEach(function(property, index) {
                if ((property === 'woofer') || (property === 'tweeter')) {
                    self.isKeyPresent(testCase[property], 'biquads').then(function() {
                        Object.keys(testCase[property].biquads).forEach(function(biquadId) {
                            self.isKeyPresent(testCase[property].biquads[biquadId], 'switch').then(function() {}, function() {
                                testCase[property].biquads[biquadId].switch = 'on';
                            }).then(function() {
                                if (testCase[property].biquads[biquadId].switch.toLowerCase() === 'off') {
                                    self.switchOffBiquad(biquadId, property).then(function() {});
                                } else {
                                    self.setBiquadValue(biquadId, property, testCase[property]).then(function() {}, function(reason) {
                                        defer.reject(reason);
                                    });
                                }
                            })
                        })
                    }, function() {});
                }
            })
        }).then(function() {
            defer.fulfill();
        })
        return defer.promise;
    }
}
var spatializer = require('./spatializerUtils.js');
var commonUtils = require('../commonUtils.js');
var fs = require('fs');
var path = require('path');
var chai = require('chai');
var expect = chai.expect;
var testInputs = require('./spatializer_testcases.json');

module.exports = {
    test: function(rootPath, testConfigJson, audioPlayer, currentConfig, audioModeIndex, currentAudioMode) {
        var defer = protractor.promise.defer();
        testCases = commonUtils.findTestCasesToRun(testInputs.testCases, testConfigJson, 'spatializer');
        var isComponentPresent = false;
        it('should open Spatializer', function() {
            commonUtils.switchOnComponent('Spatializer').then(function(componentIndex) {
                commonUtils.openComponent(componentIndex).then(function() {
                    isComponentPresent = true;
                });
            }, function error(reason) {
                isComponentPresent = false;
                console.log(reason);
            }).then(function() {
                expect(isComponentPresent, 'Spatializer not present').to.be.true;
            });

        })
        testCases.forEach(function(testCase, testCaseIndex) {
            it('should write ' + testCase.id + ' to spatializer', function() {
                expect(isComponentPresent, 'Spatializer not present').to.be.true;
                if (isComponentPresent) {
                    spatializer.turnOnSpatializer();
                    spatializer.applyTestCase(testCase).then(function success() {
                        commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio);
                        commonUtils.dumpAudioFile(path.dirname(testConfigJson.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'A', testConfigJson.appToTest);
                    }, function error(reason) {
                        console.log(reason + ' TestCase : ' + testCase.id + ' not applied.');
                    })
                    commonUtils.applySnapshot();
                    commonUtils.isDeviceBAvailable().then(function success() {
                        commonUtils.switchDevice('B');
                        spatializer.turnOnSpatializer();
                        spatializer.applyTestCase(testCase).then(function success() {
                            commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio);
                            commonUtils.dumpAudioFile(path.dirname(testConfigJson.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'A', testConfigJson.appToTest);
                        }, function error(reason) {
                            console.log(reason + ' TestCase : ' + testCase.id + ' not applied for device B.');
                        })
                        commonUtils.applySnapshot();
                        commonUtils.switchDevice('A');
                    }, function error(reason) {
                        console.log(reason + ' TestCase : ' + testCase.id + ' not applied for device B.');
                    });
                }
            })
        })

        it("should navigate back to audioprocessing page if component is present", function() {
            expect(isComponentPresent).to.be.true;
            if (isComponentPresent) {
                commonUtils.goBackToAudioProcessingPage();
                defer.fulfill();
            }
        })
        return defer.promise;
    }
}
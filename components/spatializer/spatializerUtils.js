var fs = require('fs');
var chai = require('chai');
var expect = chai.expect;
var equalizer = require('../equalizer/equalizer_Utils.js');
var commonUtils = require('../commonUtils.js');
var volume = require('../volume/volumeUtils.js');
var waitAbsent = require('../lib/waitAbsent.js');
var loopAsync = require('../lib/loopAsync.js');
module.exports = {
    /**
     * sets the properties of the basic mode.
     * @param filterName name of the filter(high pass or low pass).
     * @param property name of the property whose value has to be changed. 
     * @param value value to set the specified property.
     */
    setBasicModeProperty: function(filterName, property, value) {
        element.all(by.id('spatializer-basic-controls')).first().all(by.repeater('(key,biquad) in appData.spatializerData.bqGrp.biquads')).reduce(function(isValueSet, innerControl, innerControlIndex) {
            if (isValueSet) {
                return true;
            }
            return innerControl.all(by.css('.field-heading')).first().getText().then(function(filterText) {
                if (filterText.toLowerCase() === filterName.toLowerCase()) {
                    return innerControl.all(by.css('.input-field-holder')).reduce(function(isPropertySet, propertyElement, propertyIndex) {
                        if (isPropertySet) {
                            return true;
                        }
                        return propertyElement.all(by.css('.field-heading')).first().getText().then(function(propertyName) {
                            if (propertyName.toLowerCase() === property.toLowerCase()) {
                                propertyElement.all(by.css('.input-txt')).first().clear();
                                propertyElement.all(by.css('.input-txt')).first().sendKeys(value);
                                return true;
                            }
                        })
                    })
                }
            })
        })
    },
    /**
     * sets the level
     * @param value value to be set to the value
     */
    setLevel: function(value) {
        element.all(by.css('.spatializer-basic-right-holder')).first().all(by.css('.digigain-vertical-slider-value')).first().click();
        browser.wait(function() {
            return element.all(by.css('.spatializer-basic-right-holder')).first().all(by.css('.digigain-vertical-slider-txtbox-holder.level-width-adg')).first().isDisplayed();
        }, 50000, 'Waiting for the level meter text box to appear');
        element.all(by.css('.spatializer-basic-right-holder')).first().all(by.css('.digigain-vertical-slider-txtbox')).first().clear();
        element.all(by.css('.spatializer-basic-right-holder')).first().all(by.css('.digigain-vertical-slider-txtbox')).first().sendKeys(value);
        element.all(by.css('.spatializer-basic-right-holder')).first().click();
    },
    /**
     * changes the mode.
     * @param mode name of the mode.
     */
    changeMode: function(mode) {
        element.all(by.css('.two-way-switch-container.equal-width-switch')).first().all(by.css('.two-way-switch-holder')).reduce(function(isModeChanged, modeElement) {
            if (isModeChanged) {
                return;
            }
            return modeElement.getText().then(function(modeName) {
                if (modeName.toLowerCase() === mode.toLowerCase()) {
                    modeElement.getAttribute('class').then(function(classValue) {
                        if (classValue.indexOf('active') < 0) {
                            modeElement.click();
                            element.all(by.css('.custom-popup-btns')).reduce(function(isPopupSubmitted, popupElem) {
                                if (isPopupSubmitted) {
                                    return;
                                }
                                return popupElem.isDisplayed().then(function(displayed) {
                                    if (displayed) {
                                        return popupElem.getText().then(function(option) {
                                            if (option.toLowerCase() === "ok") {
                                                popupElem.click();
                                                return true;
                                            }
                                        })
                                    }
                                })
                            })
                        }
                        return true;
                    })
                }
            })
        })
    },
    /**
     * Checks if biquad is available.
     * Resolves when given biquad is available.
     * Rejects if biquad is not available.
     * @param biquadId biquad number relative to the UI (e.g 1)
     */
    isBiquadAvailable: function(biquadId) {
        var defer = protractor.promise.defer();
        element.all(by.css('.spatializer-biquad-tiles-holder')).reduce(function(isBiquadAvailable, biquadContainer) {
            if (isBiquadAvailable) {
                return isBiquadAvailable;
            }
            return biquadContainer.all(by.css('.deq-new-biquad-tiles-nobox')).first().getText().then(function(id) {
                if (id === biquadId) {
                    return true;
                }
            })
        }).then(function(isBiquadAvailable) {
            if (isBiquadAvailable) {
                defer.fulfill();
            } else {
                defer.reject('Biquad : ' + biquadId + ' not present.');
            }
        })
        return defer.promise;
    },
    /**
     * sets the filter type in advanced mode.
     * @param biquadId id of the biquad
     * @param biquad An object having the test case in the format of
     *                  {
     *                      id: { 
     *                              property : value
     *                          }
     *                   }
     */
    setFilterType: function(biquadId, biquad) {
        var self = module.exports;
        var defer = protractor.promise.defer();
        var current_biquad = element.all(by.css('.spatializer-biquad-tiles-holder')).get(biquadId - 1);
        self.isBiquadAvailable(biquadId).then(function() {
            equalizer.applyFilterType(current_biquad, biquad, biquadId, 'biquad.props.filter').then(function success() {
                defer.fulfill();
            })
        }, function(reason) {
            defer.reject(reason);
        });
        return defer.promise;
    },
    /**
     * sets the properties of the advanced mode.
     * @param biquadId id of the biquad
     * @param biquad An object having the test case in the format of
     *                  {
     *                      id: { 
     *                              property : value
     *                          }
     *                   }
     */
    setAdvancedModeProperties: function(biquadId, biquad) {
        var current_biquad = element.all(by.css('.spatializer-biquad-tiles-holder')).get(biquadId - 1);
        equalizer.applyKeyInputs(current_biquad, biquad, biquadId).then(function() {});
    },
    /**
     * switches off the biquad.
     * @param biquadId id of the biquad
     */
    switchOffBiquad: function(biquadId) {
        var self = module.exports;
        var defer = protractor.promise.defer();
        self.isBiquadAvailable(biquadId).then(function() {
            element.all(by.css('.spatializer-biquad-tiles-holder')).get(biquadId - 1).all(by.css('.on-switch')).first().click();
            defer.fulfill();
        }, function(reason) {
            defer.reject(reason);
        })
        return defer.promise;
    },
    /**
     * checks whether the given key is present in the object or not.
     * @param obj object to check for the presence of key.
     * @param key key to be checked for its presence in the obj.
     */
    isKeyPresent: function(obj, key) {
        if (Object.keys(obj).indexOf(key) > -1) {
            return true;
        }
        return false;
    },
    /**
     * Turns on spatializer in spatializer page.
     */
    turnOnSpatializer: function() {
        element.all(by.css('.spatializer-top-ctrls-holder')).first().all(by.css('.off-switch')).first().isDisplayed().then(function(displayed) {
            if (displayed) {
                element.all(by.css('.spatializer-top-ctrls-holder')).first().all(by.css('.off-switch')).first().click();
                browser.wait(function() {
                    return element.all(by.css('.spatializer-top-ctrls-holder')).first().all(by.css('.off-switch')).first().waitAbsent();
                }, 20000, 'Waiting to switch on the spatializer');
            }
        })
    },
    /**
     * The main function which is getting called from the spatializer_spec. 
     * Applies the given test case.
     * @param testCase test case object.
     */
    applyTestCase: function(testCase) {
        var self = module.exports;
        var defer = protractor.promise.defer();
        if (!self.isKeyPresent(testCase, 'leftVol')) {
            testCase.leftVol = -6;
        }
        if (!self.isKeyPresent(testCase, 'rightVol')) {
            testCase.rightVol = -10;
        }
        volume.setVolume(testCase.leftVol, testCase.rightVol);
        if (self.isKeyPresent(testCase, 'level')) {
            self.setLevel(testCase.level);
        }
        if (self.isKeyPresent(testCase, 'mode')) {
            testCase.mode.forEach(function(mode) {
                if (!self.isKeyPresent(mode, 'name')) {
                    mode.name = 'basic';
                }
                self.changeMode(mode.name);
                if (mode.name.toLowerCase() === 'advanced') {
                    if (self.isKeyPresent(mode, 'biquads')) {
                        loopAsync(Object.keys(mode.biquads).length, function(loop) {
                            var biquadId = Object.keys(mode.biquads)[loop.iteration()];
                            if (!self.isKeyPresent(mode.biquads[biquadId], 'switch')) {
                                mode.biquads[biquadId].switch = 'on';
                            }
                            if (mode.biquads[biquadId].switch.toLowerCase() === 'off') {
                                self.switchOffBiquad(biquadId).then(function() {
                                    loop.next();
                                }, function(reason) {
                                    loop.break();
                                })
                            } else if (mode.biquads[biquadId].switch.toLowerCase() === 'on') {
                                if (self.isKeyPresent(mode.biquads[biquadId], 'filterType')) {
                                    self.setFilterType(biquadId, mode.biquads).then(function success() {
                                        if (Object.keys(mode.biquads[biquadId]).length > 2) {
                                            self.setAdvancedModeProperties(biquadId, mode.biquads);
                                            loop.next();
                                        }
                                    }, function error(reason) {
                                        loop.break();
                                    })
                                } else {
                                    if (Object.keys(mode.biquads[biquadId]).length > 1) {
                                        self.setAdvancedModeProperties(biquadId, mode.biquads);
                                        loop.next();
                                    }
                                }
                            } else {
                                console.log('Mention the correct switching value. Switch can have value as either on or off.');
                                loop.break();
                            }
                        }, function() {
                            defer.fulfill();
                        });
                    }
                } else if (mode.name.toLowerCase() === 'basic') {
                    if (self.isKeyPresent(mode, 'filters')) {
                        mode.filters.forEach(function(filter) {
                            if (!self.isKeyPresent(filter, 'type')) {
                                filter.type = 'Low Pass Filter';
                            }
                            if (Object.keys(filter).length > 1) {
                                Object.keys(filter).forEach(function(property) {
                                    if (property !== 'type') {
                                        self.setBasicModeProperty(filter.type, property, filter[property]);
                                        defer.fulfill();
                                    }
                                })
                            } else {
                                defer.fulfill();
                            }
                        })
                    }
                }
            })
        } else {
            defer.fulfill();
        }
        return defer.promise;
    }
}
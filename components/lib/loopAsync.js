var loopAsync = function (iters, proc, exitCb){
    var count = 0,
        done = false,
        shouldExit = false;
    var loop = {
        iteration:function(){
            return count - 1;
        },
        next:function(){
            if(done){
                if(shouldExit && exitCb){
                    exitCb();
                }
                return;
            }
            if(count < iters){
                count++;
                proc(loop);
            } else {
                done = true; 
                if(exitCb) exitCb();
            }
        },
        break:function(end){
            done = true;
            shouldExit = end;
        }
    };
    loop.next();
    return loop;
};

var whileAsync = function (conditionCallback, proc, exitCb){
    var count = 0,
        done = false,
        shouldExit = false;
    var loop = {
        iteration:function(){
            return count - 1;
        },
        next:function(){
            if(done){
                if(shouldExit && exitCb){
                    exitCb();
                }
                return;
            }
            if(conditionCallback()){
                count++;
                proc(loop);
            } else {
                done = true;
                if(exitCb) exitCb();
            }
        },
        break:function(end){
            done = true;
            shouldExit = end;
        }
    };
    loop.next();
    return loop;
};

//Exported for using in e2e tests
if(typeof module !="undefined" && module.exports){
	module.exports = loopAsync;
}
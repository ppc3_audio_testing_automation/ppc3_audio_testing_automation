var millisToMinutesAndSeconds = function(millis) {
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
};
module.exports = {
    "checkConsole": function() {
        var erroroccur = false;
        browser.manage().logs().get('browser').then(function(browserLog) {
            if (browserLog.length > 0) {
                browserLog.forEach(function(log) {
                    if (log.level.value > 900) {
                        browser.params.console.push(log.message);
                        erroroccur = true;
                    }
                });
            }
            if (!erroroccur) {
                browser.params.console.push('none');
            }
            // var date = new Date();
            // browser.params.time.push(date.getHours() + ' : ' + date.getMinutes() + ' : ' + date.getSeconds());
        });
    },
    "generateExcel": function(filename) {
        var fs = require('fs');
        if (fs.existsSync('./output_files/report.json')) {
            var json = JSON.parse(fs.readFileSync('./output_files/report.json').toString());
            var json2xls = require('json2xls');
            json.forEach(function(element, index) {
                if (element.assertions[0].passed === false) {
                    json[index].status = 'failed';
                    json[index].fail_message = json[index].assertions[0].errorMsg;
                    json[index].stack_trace = json[index].assertions[0].stackTrace;
                } else {
                    json[index].status = 'passed';
                    json[index].fail_message = '--';
                    json[index].stack_trace = '--';
                }
                json[index].durationInMinutes = millisToMinutesAndSeconds(json[index].duration);
                json[index].id = index + 1;
                json[index].description = 'Audio testing automation ' + json[index].description;
                json[index].console_error = browser.params.console[index];
            });
            xls = json2xls(json, {
                fields: ['id', 'description', 'durationInMinutes', 'status', 'fail_message', 'stack_trace', 'console_error']
            });
            fs.writeFileSync('./output_files/Report_of_' + filename + '.xlsx', xls, 'binary');
        } else {
            console.log('Error : Json report has not been created.\n ./output_files/report.json does not exist');
        }
    }
};
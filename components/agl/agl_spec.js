var agl = require('./agl_utils.js');
var chai = require('chai');
var expect = chai.expect;
var path = require('path');
var commonUtils = require('../commonUtils.js');
module.exports = {
    /**
     * Tests the AGL.
     * @param rootPath Current working directory path.
     * @param runInput Contents of config.json file.
     * @param audioPlayer audio player component to call its functions.
     * @param currentConfig Name of the configurations which is currently opened.
     * @param currentAudioMode Name of the audio mode which is currently opened.
     */

    test: function(rootPath, runInput, audioPlayer, currentConfig, currentAudioMode) {
        var defer = protractor.promise.defer();
        var isComponentPresent = false;
        var isSpeakerNotCombined = false;
        it('should open AGL', function() {
            agl.switchOnAgl().then(function() {
                agl.isSpeakerNotCombined().then(function() {
                    isSpeakerNotCombined = true;
                }, function() {})
                isComponentPresent = true;
                agl.openAgl();
            }, function() {}).then(function() {
                expect(isComponentPresent, 'AGL not present').to.be.true;
            })
        })
        var testInputs = require('./agl_testcases.json');
        var testCases = commonUtils.findTestCasesToRun(testInputs.testCases, runInput, 'agl');
        testCases.forEach(function(testCase, testCaseIndex) {
            it('should write test case :' + testCase.id, function() {
                expect(isComponentPresent, 'AGL not present').to.be.true;
                if (isComponentPresent) {
                    agl.turnOnAgl();
                    agl.applyTestCase(testCase);
                    var deviceType;
                    if (isSpeakerNotCombined) {
                        deviceType = 'Woofer_A';
                    } else {
                        deviceType = 'A';
                    }
                    commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                        commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, deviceType, runInput.appToTest);
                    })
                    commonUtils.applySnapshot();

                    commonUtils.isDeviceBAvailable().then(function() {
                        commonUtils.switchDevice('B');
                        agl.turnOnAgl();
                        agl.applyTestCase(testCase);
                        commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                            commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'B', runInput.appToTest);
                        })
                        commonUtils.applySnapshot();
                        commonUtils.switchDevice('A');
                    }, function() {}).then(function() {
                        if (isSpeakerNotCombined) {
                            commonUtils.goBackToAudioProcessingPage();
                            commonUtils.switchToSpeaker('Tweeter');
                            agl.switchOnAgl().then(function() {
                                agl.openAgl();
                                agl.applyTestCase(testCase);
                                commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                                    commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'Tweeter_A', runInput.appToTest);
                                })
                                commonUtils.applySnapshot();
                                commonUtils.goBackToAudioProcessingPage();
                                commonUtils.switchToSpeaker('Woofer');
                                agl.openAgl();
                            });
                        }
                    }).then(function() {
                        if (testCaseIndex === testCases.length - 1) {
                            commonUtils.goBackToAudioProcessingPage();
                            defer.fulfill();
                        }
                    })
                }
            })
        })
        return defer.promise;
    }
}
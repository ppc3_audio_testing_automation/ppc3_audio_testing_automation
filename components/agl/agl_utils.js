var waitAbsent = require('../lib/waitAbsent.js');
module.exports = {
    /**
     * switches on AGL in audio processing page.
     * Promise resolves if component is present.
     * Promise gets rejected if component is not present.
     */
    "switchOnAgl": function() {
        var defer = protractor.promise.defer();
        element.all(by.css('.headsection')).reduce(function(isAglPresent, header) {
            if (isAglPresent) {
                return isAglPresent;
            }
            return header.all(by.css('.tile-headings-common')).first().getText().then(function(title) {
                if (title.toLowerCase().indexOf('agl') > -1) {
                    return header.all(by.css('.off-switch')).first().click().then(function() {
                        return element(by.id('AGL-on')).isPresent().then(function(present) {
                            if (present) {
                                element(by.id('AGL-on')).isDisplayed().then(function(displayed) {
                                    if (displayed) {
                                        element(by.id('AGL-on')).click();
                                        browser.wait(function() {
                                            return element(by.id('AGL-on')).waitAbsent();
                                        }, 10000, 'Waiting for overlay to disappear.')
                                    }
                                })
                            }
                            return true;
                        })
                    })
                }
            })
        }).then(function(isAglPresent) {
            if (isAglPresent) {
                defer.fulfill();
            } else {
                defer.reject();
            }
        })
        return defer.promise;
    },
    /**
     * Opens the agl page.
     */
    "openAgl": function() {
        element.all(by.css('div[title="Click to view Full Band AGL page"]')).reduce(function(isOpened, zoomButton) {
            if (isOpened) {
                return;
            }
            return zoomButton.isDisplayed().then(function(displayed) {
                if (displayed) {
                    zoomButton.click();
                    return true;
                }
            })
        });
        browser.wait(function() {
            return element.all(by.css('.breadscrum-link')).get(3).isDisplayed();
        }, 50000, 'Waiting for the component page.');
    },
    /**
     * Sets the properties of Input/Output block.
     * @param property name of the property whose value is to be changed.
     * @param value Value to change the specified property.
     */
    "setIOProperties": function(property, value) {
        element.all(by.css('.fb-agl-btm-left-holder')).first().all(by.css('.fb-agl-btm-controls-holder')).reduce(function(isPropFound, propElement) {
            if (isPropFound) {
                return;
            }
            return propElement.all(by.css('.nominal-loudness-field-titles')).first().getText().then(function(propName) {
                if (propName.toLowerCase() === property.toLowerCase()) {
                    propElement.all(by.css('.nw-gdata-input-ctrl')).first().clear();
                    propElement.all(by.css('.nw-gdata-input-ctrl')).first().sendKeys(value);
                    return true;
                }
            })
        })
    },
    /**
     * Sets the properties of attack/release block.
     * @param property Name of the property whose value is to be changed.
     * @param value Value to change the specified property.
     */
    "setTimeConstantProperties": function(property, value) {
        element.all(by.css('.fb-agl-btm-right-holder')).first().all(by.css('.fb-agl-btm-controls-holder')).reduce(function(isPropFound, propElement) {
            if (isPropFound) {
                return;
            }
            return propElement.all(by.css('.nominal-loudness-field-titles')).first().getText().then(function(propName) {
                if (propName.toLowerCase() === property.toLowerCase()) {
                    propElement.all(by.css('.nw-gdata-input-ctrl')).first().clear();
                    propElement.all(by.css('.nw-gdata-input-ctrl')).first().sendKeys(value);
                    element(by.css('.agl-container-overlay')).isDisplayed().then(function(displayed) {
                        if (displayed) {
                            browser.wait(function() {
                                return element(by.css('.agl-container-overlay')).waitAbsent();
                            }, 50000, 'Waiting for rescaling overlay to disappear.');
                        }
                    })
                    return true;
                }
            })
        })
    },
    /**
     * Applies the given testCase.
     * @param testCase testCase object to get applied to AGL.
     */
    "applyTestCase": function(testCase) {
        var self = this;
        if (self.isKeyPresent(testCase, 'io')) {
            Object.keys(testCase.io).forEach(function(property) {
                self.setIOProperties(property, testCase.io[property]);
            })
        }
        if (self.isKeyPresent(testCase, 'attackOrRelease')) {
            Object.keys(testCase.attackOrRelease).forEach(function(property) {
                self.setTimeConstantProperties(property, testCase.attackOrRelease[property]);
            })
        }
    },
    /**
     * Checks whether the given key is present in the given object or nor.
     * @param obj Object to check the presence of given key in it.
     * @param key Name of the key to check for its presence in the given object.
     */
    "isKeyPresent": function(obj, key) {
        if (Object.keys(obj).indexOf(key) > -1) {
            return true;
        }
        return false;
    },
    /**
     * Turns on AGL in agl page.
     */
    "turnOnAgl": function() {
        element(by.css('div[title="Switch On AGL"]')).isDisplayed().then(function(displayed) {
            if (displayed) {
                element(by.css('div[title="Switch On AGL"]')).click();
                browser.wait(function() {
                    return element.all(by.css('.disable-overlay-agl-right overlay-on-total')).first().waitAbsent();
                }, 50000, 'Waiting for overlay of agl to disappear.');
            }
        })
    },
    /**
     * Checks whether agl is combined for both woofer and tweeter or not.
     * Resolves when agl is separate for woofer and tweeter.
     * Rejects when agl is common for woofer and tweeter.
     */
    "isSpeakerNotCombined": function() {
        var defer = protractor.promise.defer();
        var tileIndex;
        element(by.id('rb-grid-left')).isPresent().then(function(displayed) {
            if (displayed) {
                element(by.id('rb-grid-left')).all(by.css(".headsection")).reduce(function(result, elm, index) {
                    if (result > -1) {
                        return result;
                    }
                    return elm.all(by.css(".tile-headings-common")).first().getText().then(function(text) {
                        if (text.toLowerCase().indexOf('agl') > -1) {
                            tileIndex = index;
                            return tileIndex;
                        }
                    });
                }).then(function(tileIndex) {
                    if (tileIndex === undefined) {
                        defer.fulfill();
                    }
                })
            } else {
                defer.reject();
            }
        })
        return defer.promise;
    }
}
/**
 * This file has the utils functions for the DPEQ component
 * testDPEQComponent       - This is the function which will run the test for a single Device (A or B)
 * applyThreshold settings - this will apply the threshold values like,
 *                                      Low Threshold
 *                                      High Threshold
 *                                      Energy 
 *                         - if those values are specified it will get apply
 * 
 * switchOnComponent - This function will switch on the component if it is in the component page
 * switchMode        - This function will switch between the two modes basic and Advanced
 * applyEnergySenseInputs     - This function will call the other functions which will apply inputs to Energy Sense, Low Level EQ & High Level EQ
 * applyBasicEnergySenseInput - This function will apply the Basic energy sense inputs if advanced option is not specified in the test case
 * applyBiquadInput           - This function will apply the inputs for the biquads which will make use of the equalizer utils functionalities
 * changePanel                - This will switch between the panels (Energy Sense , Low Level EQ, High Level EQ)
 * findNoOfBiquads            - This function will return the total no of biquads in the UI
 * isAllBiquadsAvailable      - This function will be fulfilled if all the biquads are present in the UI which are specified in the testcase
 * ifTestCaseIsApplicable     - This function will be fulfilled if the test case is valid. 
 */
var eql_utils = require('../equalizer/equalizer_Utils.js');
var volume = require('../volume/volumeUtils.js');
var common_utils = require('../commonUtils.js');
var audio_player = require('../audio_player/audio_player_operations.js');
var loopAsync = require('../lib/loopAsync.js');
var path = require('path');
module.exports = {
    "applyThresholdSettings": function(testCaseObject) {
        var defer = protractor.promise.defer();
        if (testCaseObject.thresholdLow !== undefined || testCaseObject.thresholdLow !== null) {
            element.all(by.model('appData.dpeqData.props.thresholdLow.value')).first().clear();
            element.all(by.model('appData.dpeqData.props.thresholdLow.value')).first().sendKeys(testCaseObject.thresholdLow);
        }
        if (testCaseObject.thresholdHigh !== undefined || testCaseObject.thresholdHigh !== null) {
            element.all(by.model('appData.dpeqData.props.thresholdHigh.value')).first().clear();
            element.all(by.model('appData.dpeqData.props.thresholdHigh.value')).first().sendKeys(testCaseObject.thresholdHigh);
        }
        if (testCaseObject.energy !== undefined || testCaseObject.energy !== null) {
            element.all(by.model('appData.dpeqData.props.energy.value')).first().clear();
            element.all(by.model('appData.dpeqData.props.energy.value')).first().sendKeys(testCaseObject.energy).then(function() {
                defer.fulfill();
            });
        } else {
            defer.fulfill();
        }
        return defer.promise;
    },
    "switchOnComponent": function() {
        var defer = protractor.promise.defer();
        browser.wait(function() {
            return element.all(by.css('.dpq-top-controls-holder.deq')).first().isDisplayed();
        }, 50000, 'waiting for the holder div element');
        element.all(by.css('.dpq-top-controls-holder.deq')).first().all(by.css('.off-switch')).first().isDisplayed().then(function(displayed) {
            if (displayed) {
                element.all(by.css('.dpq-top-controls-holder.deq')).first().all(by.css('.off-switch')).first().click().then(function() {
                    defer.fulfill();
                });
            } else {
                defer.fulfill();
            }
        });
        return defer.promise;
    },
    "switchMode": function(mode) {
        var defer = protractor.promise.defer();
        element.all(by.css('.deq-new-basic-advanced-tab-holder')).first().all(by.css('.deq-new-basic-advanced-tab')).each(function(tab) {
            tab.getText().then(function(text) {
                if (text === mode) {
                    tab.click().then(function() {
                        element.all(by.css('.custom-popup-container')).each(function(popup) {
                            popup.isDisplayed().then(function(displayed) {
                                if (displayed) {
                                    popup.all(by.css('.custom-popup-btns')).first().click();
                                }
                            });
                        });
                    });
                }
            });
        }).then(function() {
            defer.fulfill();
        });
        return defer.promise;
    },
    "applyBasicEnergySenseInput": function(testCaseObject) {
        var defer = protractor.promise.defer();
        module.exports.switchMode('Basic').then(function() {
            var current_biquad = element.all(by.css('.deq-new-bas-ad-tab-left-holder')).first();
            eql_utils.applyFilterType(current_biquad, testCaseObject.energySense, 'basic', 'appData.dpeqData.props.senseEqFilterType.value').then(function() {
                eql_utils.applyKeyInputs(current_biquad, testCaseObject.energySense, 'basic').then(function() {
                    defer.fulfill();
                });
            });
        });
        return defer.promise;
    },
    "applyEnergySenseInputs": function(testCaseObject) {
        var defer = protractor.promise.defer();
        if (testCaseObject.energySense.advanced !== undefined) {
            module.exports.applyBiquadInput(testCaseObject.energySense.advanced, 'Energy Sense Advanced').then(function() {
                defer.fulfill();
            }, function() {
                defer.reject();
            });
        } else if (testCaseObject.energySense.basic !== undefined) {
            module.exports.changePanel('Energy Sense').then(function() {
                module.exports.applyBasicEnergySenseInput(testCaseObject).then(function() {
                    defer.fulfill();
                }, function() {
                    defer.reject();
                });
            });
        } else if (testCaseObject.energySense.advanced === undefined) {
            module.exports.applyBiquadInput(testCaseObject.energySense.biquads, 'Energy Sense').then(function() {
                defer.fulfill();
            }, function() {
                defer.reject();
            });
        }
        return defer.promise;
    },
    "findNoOfBiquads": function() {
        var defer = protractor.promise.defer();
        element.all(by.css('.deq-new-biquad-tiles-container')).first().isPresent().then(function(displayed) {
            if (displayed) {
                element.all(by.css('.deq-new-biquad-tiles-container')).first().all(by.repeater('biquad in appData.dpeqData.bqGrps.senseEq.biquads')).count().then(function(count) {
                    defer.fulfill(count);
                });
            } else {
                element.all(by.css('.deq-new-rpanel-tiles-container-order2')).first().isPresent().then(function(displayed) {
                    if (displayed) {
                        element.all(by.css('.deq-new-rpanel-tiles-container-order2')).first().all(by.repeater('biquad in appData.dpeqData.bqGrps.senseEq.biquads')).count().then(function(count) {
                            defer.fulfill(count);
                        });
                    }
                });
            }
        });
        return defer.promise;
    },
    "changePanel": function(mode) {
        var defer = protractor.promise.defer();
        var panelIndex = -1;
        mode = mode.toLowerCase().split(' ').join('');
        element.all(by.css('.deq-new-rpanels-tab-header')).first().all(by.css('.deq-new-rpanels-tab')).each(function(panel, panel_index) {
            panel.getText().then(function(panel_name) {
                panel_name = panel_name.toLowerCase().split(' ').join('');
                if (panel_name === mode) {
                    element.all(by.css('.deq-new-rpanels-tab-header')).first().all(by.css('.deq-new-rpanels-tab.active')).first().getText().then(function(text) {
                        text = text.toLowerCase().split(' ').join('');
                        if (text !== mode) {
                            element.all(by.css('.deq-new-rpanels-tab-header')).first().all(by.css('.deq-new-rpanels-tab')).get(panel_index).click();
                        }
                    });
                }
            });
        }).then(function() {
            defer.fulfill();
        });
        return defer.promise;
    },
    "isAllBiquadsAvailable": function(testCaseForEQ, model, level) {
        var defer = protractor.promise.defer();
        var isAvailable = false;
        var biquads = Object.keys(testCaseForEQ);
        var index = -1;
        if (level === 'lowleveleq')
            index = 0;
        else if (level === 'highleveleq')
            index = 1;
        var skipTest = false;
        element.all(by.css('.deq-new-rpanel-tiles-container-order2')).get(index).all(by.repeater(model)).each(function(biquad_dom, result) {
            biquad_dom.all(by.css('.deq-new-biquad-tiles-nobox')).first().getText().then(function(text) {
                if (biquads.indexOf(text) === -1) {
                    skipTest = true;
                }
            });
        }).then(function() {
            if (skipTest === false)
                defer.fulfill();
            else if (skipTest === true)
                defer.reject();
        });
        return defer.promise;
    },
    "applyBiquadInput": function(testCaseForEQ, level) {
        var defer = protractor.promise.defer();
        var model, newlevel;
        level = level.toLowerCase().split(' ').join('');
        if (level.indexOf('advanced') > -1) {
            newlevel = level.replace('advanced', '');
        } else {
            newlevel = level;
        }
        if (level === 'lowleveleq') {
            model = 'biquad in appData.dpeqData.bqGrps.lowLevelEq.biquads';
        } else if (level === 'highleveleq') {
            model = 'biquad in appData.dpeqData.bqGrps.highLevelEq.biquads';
        } else if (level === 'energysenseadvanced' || level === 'energysense') {
            model = 'biquad in appData.dpeqData.bqGrps.senseEq.biquads';
        }
        module.exports.changePanel(newlevel).then(function() {
            module.exports.isAllBiquadsAvailable(testCaseForEQ, model, level).then(function() {
                loopAsync(Object.keys(testCaseForEQ).length, function(loop) {
                    var biquad = Object.keys(testCaseForEQ)[loop.iteration()];
                    var current_biquad;
                    if (level !== 'energysense' && level !== 'energysenseadvanced')
                        current_biquad = element.all(by.repeater(model)).get(loop.iteration());
                    else if (level === 'energysenseadvanced') {
                        current_biquad = element.all(by.css('.deq-new-bas-ad-tab-right-holder')).first().all(by.repeater('biquad in appData.dpeqData.bqGrps.senseEq.biquads')).get(loop.iteration());
                    } else if (level === 'energysense') {
                        current_biquad = element.all(by.css('.deq-new-rpanel-btm-holder')).first();
                    }
                    eql_utils.switchOnBiquad(current_biquad).then(function() {
                        eql_utils.applyFilterType(current_biquad, testCaseForEQ, biquad, 'biquad.props.filter').then(function() {
                            eql_utils.applyKeyInputs(current_biquad, testCaseForEQ, biquad).then(function() {
                                loop.next();
                            });
                        });
                    });
                });
            }, function() {
                defer.reject();
            });
        }).then(function() {
            defer.fulfill();
        });
        return defer.promise;
    },
    "testDPEQComponent": function(testCaseObject, runInput, rootPath, config, audioModeElement, device, defaultAudioInput) {
        var defer = protractor.promise.defer();
        if (config.indexOf('/') > -1) {
            config = config.replace('/', '');
        }
        module.exports.applyThresholdSettings(testCaseObject).then(function() {
            volume.setVolume(testCaseObject.volume, testCaseObject.volume).then(function() {
                module.exports.applyEnergySenseInputs(testCaseObject).then(function() {
                    module.exports.applyBiquadInput(testCaseObject.lowLevelEQ, 'Low Level EQ').then(function() {
                        module.exports.applyBiquadInput(testCaseObject.highLevelEQ, 'High Level EQ').then(function() {
                            common_utils.recordAudio(rootPath, audio_player, testCaseObject, defaultAudioInput).then(function() {
                                common_utils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCaseObject.id, config, audioModeElement, device, runInput.appToTest);
                                defer.fulfill();
                            });
                        }, function() {
                            defer.reject();
                        });
                    }, function() {
                        defer.reject();
                    });
                }, function() {
                    defer.reject();
                });
            });
        }).then(function() {
            defer.fulfill();
        });
        return defer.promise;
    },
    "ifTestCaseIsApplicable": function(testCaseObject) {
        var defer = protractor.promise.defer();
        var noOfBiquads = -1;
        module.exports.findNoOfBiquads().then(function(biquad_count) {
            noOfBiquads = biquad_count;
        });
        element.all(by.css('.deq-new-basic-advanced-tab-holder')).first().isPresent().then(function(presenceOfAdvancedTab) {
            if (presenceOfAdvancedTab) {
                if (testCaseObject.energySense.advanced !== undefined) {
                    if (noOfBiquads !== Object.keys(testCaseObject.energySense).length || noOfBiquads !== Object.keys(testCaseObject.lowLevelEQ).length || noOfBiquads !== Object.keys(testCaseObject.highLevelEQ).length) {
                        defer.reject();
                    } else {
                        defer.fulfill();
                    }
                } else {
                    if (testCaseObject.energySense.basic !== undefined) {
                        defer.fulfill();
                    } else {
                        defer.reject();
                    }
                }
            } else {
                if (testCaseObject.energySense.biquads !== undefined) {
                    if (Object.keys(testCaseObject.energySense.biquads).length > -1) {
                        module.exports.findNoOfBiquads().then(function(noOfBiquad) {
                            if (noOfBiquads !== Object.keys(testCaseObject.energySense.biquads).length || noOfBiquads !== Object.keys(testCaseObject.lowLevelEQ).length || noOfBiquads !== Object.keys(testCaseObject.highLevelEQ).length) {
                                defer.reject();
                            } else {
                                defer.fulfill();
                            }
                        });
                    } else {
                        defer.reject();
                    }
                } else {
                    defer.reject();
                }
            }
        });
        return defer.promise;
    }
};
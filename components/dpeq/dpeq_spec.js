var common_utils = require('../commonUtils.js');
var testCasesJson = require('./dpeq_testcases.json');
var dpeq_utils = require('./dpeq_Utils.js');
var audio_player = require('../audio_player/audio_player_operations.js');

var chai = require('chai');
var expect = chai.expect;
var deviceB = false;
module.exports = {
    test: function(rootPath, runInput, audio_player, config, audioModePosition, audioModeElement) {
        var defer = protractor.promise.defer();
        var isComponentPresent = false;
        it('should open the DPEQ component', function() {
            browser.ignoreSynchronization = true;
            common_utils.switchOnComponent('DEQ').then(function(indexOfTheComponent) {
                common_utils.openComponent(indexOfTheComponent).then(function() {
                    isComponentPresent = true;
                });
            }, function() {
                isComponentPresent = false;
            });
        });
        var testCases = common_utils.findTestCasesToRun(testCasesJson.testCases, runInput, 'dpeq');
        testCases.forEach(function(testCaseObject) {
            it('' + testCaseObject.id, function() {
                expect(isComponentPresent, 'DPEQ Component Not Available : ').to.be.true;
                browser.ignoreSynchronization = true;
                dpeq_utils.switchOnComponent().then(function() {
                    dpeq_utils.ifTestCaseIsApplicable(testCaseObject).then(function() {
                        dpeq_utils.testDPEQComponent(testCaseObject, runInput, rootPath, config, audioModeElement, 'A', testCasesJson.defaultAudioInput).then(function() {
                            common_utils.isDeviceBAvailable().then(function() {
                                common_utils.switchDevice('B').then(function() {
                                    dpeq_utils.switchOnComponent().then(function() {
                                        dpeq_utils.testDPEQComponent(testCaseObject, runInput, rootPath, config, audioModeElement, 'B', testCasesJson.defaultAudioInput).then(function() {
                                            common_utils.switchDevice('A').then(function() {

                                            });
                                        }, function() {
                                            common_utils.switchDevice('A').then(function() {
                                                console.log('Test for Device B has been rejected!!!');
                                            });
                                        });
                                    });
                                });
                            }, function() {});
                        }, function() {
                            console.log('test for Device A has been rejected!!!');
                        });
                    }, function() {
                        console.log('not applicable');
                    });
                }).then(function() {
                    common_utils.applySnapshot();
                });
            });
        });
        it('should close the DPEQ component', function() {
            browser.wait(function() {
                return element.all(by.css('.breadscrum-link')).get(2).isDisplayed();
            }, 50000, 'waiting for breadscrum link');
            element.all(by.css('.breadscrum-link')).get(2).click().then(function() {
                common_utils.switchOffComponent("DEQ").then(function() {
                    defer.fulfill();
                });
            });
        });
        return defer.promise;
    }
};
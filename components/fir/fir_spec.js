var fs = require('fs');
var path = require('path');
var commonUtils = require('../commonUtils.js');
var firUtils = require('./fir_utils.js');
var chai = require('chai');
var expect = chai.expect;

module.exports = {
    /**
     * Tests the FIR Filter.
     * @param rootPath Current working directory path.
     * @param runInput Contents of config.json file.
     * @param audioPlayer audio player component to call its functions.
     * @param currentConfig Name of the configurations which is currently opened.
     * @param currentAudioMode Name of the audio mode which is currently opened.
     */

    test: function(rootPath, runInput, audioPlayer, currentConfig, currentAudioMode) {
        var defer = protractor.promise.defer();
        var isComponentPresent = false;
        var isNotCombined = false,
            errorMsg,
            componentIndex, isTestCaseApplied = false;
        it('should open FIR filter', function() {
            commonUtils.isSpeakerNotCombined("FIR Filter").then(function() {
                isNotCombined = true;
            }, function() {})
            commonUtils.switchOnComponent("FIR Filter").then(function(index) {
                isComponentPresent = true;
                commonUtils.openComponent(index);
            }, function() {}).then(function() {
                expect(isComponentPresent, 'Fir filter not present').to.be.true;
            })
        })
        var testInputs = require('./fir_testcases.json');
        testCases = commonUtils.findTestCasesToRun(testInputs.testCases, runInput, "fir");
        testCases.forEach(function(testCase, index) {
            it("should write test case " + testCase.id, function() {
                expect(isComponentPresent, 'Fir filter not present').to.be.true;
                if (isComponentPresent) {
                    firUtils.turnOnFir();
                    firUtils.applyTestCase(testCase).then(function() {
                        isTestCaseApplied = true;
                        commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                            var deviceType;
                            if (isNotCombined) {
                                deviceType = 'Woofer_A';
                            } else {
                                deviceType = 'A';
                            }
                            commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, deviceType, runInput.appToTest);
                        }).then(function() {
                            commonUtils.applySnapshot();
                            commonUtils.isDeviceBAvailable().then(function() {
                                commonUtils.switchDevice('B');
                                firUtils.turnOnFir();
                                firUtils.applyTestCase(testCase).then(function() {
                                    commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                                        commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'B', runInput.appToTest);
                                    });
                                }, function() {});
                                commonUtils.applySnapshot();
                                commonUtils.switchDevice('A');
                            }, function(error) {})
                        }).then(function() {
                            if (isNotCombined) {
                                commonUtils.goBackToAudioProcessingPage();
                                commonUtils.switchToSpeaker('Tweeter');
                                commonUtils.switchOnComponent("FIR Filter").then(function(index) {
                                    componentIndex = index;
                                    commonUtils.openComponent(index);
                                    firUtils.applyTestCase(testCase).then(function() {
                                        commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                                            commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'Tweeter_A', runInput.appToTest);
                                        });
                                    }, function() {}).then(function() {
                                        commonUtils.applySnapshot();
                                        commonUtils.goBackToAudioProcessingPage();
                                        commonUtils.switchToSpeaker('Woofer');
                                        commonUtils.openComponent(index);
                                    })
                                }, function() {});
                            }
                        }).then(function() {
                            if (index === testCases.length - 1) {
                                commonUtils.goBackToAudioProcessingPage();
                                defer.fulfill();
                            }
                        })
                    }, function(reason) {
                        isTestCaseApplied = false;
                        errorMsg = reason;
                        if (index === testCases.length - 1) {
                            commonUtils.goBackToAudioProcessingPage();
                            defer.fulfill();
                        }
                    }).then(function() {
                        expect(isTestCaseApplied, errorMsg).to.be.true;
                    })

                }
            })
        })
        return defer.promise;
    }
}
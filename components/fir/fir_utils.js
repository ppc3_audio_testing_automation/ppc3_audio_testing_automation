var waitAbsent = require('../lib/waitAbsent.js');
var fs = require('fs');
module.exports = {
    /**
     * Turns on FIR.
     * resolves when FIR is turned on.
     */
    "turnOnFir": function() {
        var defer = protractor.promise.defer();
        element(by.css('div[title="Enable FIR"]')).isPresent().then(function(present) {
            if (present) {
                element(by.css('div[title="Enable FIR"]')).isDisplayed().then(function(displayed) {
                    if (displayed) {
                        element(by.css('div[title="Enable FIR"]')).click();
                    }
                }).then(function() {
                    defer.fulfill();
                })
            }
        });
        return defer.promise;
    },
    /**
     * Imports coefficients for FIR.
     * @param coeffPath Path of txt file having coefficients.
     * resolves whne coefficients are imported.
     * rejects if the file path does not exist.
     */
    "importCoeff": function(coeffPath) {
        var defer = protractor.promise.defer();
        if (fs.existsSync(coeffPath)) {
            element(by.css('input[title="Click to import FIR coeffs (.txt)"]')).isPresent().then(function(present) {
                if (present) {
                    element(by.css('input[title="Click to import FIR coeffs (.txt)"]')).sendKeys(coeffPath).then(function() {
                        browser.wait(function() {
                            return element(by.css('div[title="App status will be shown here"]')).isPresent();
                        }, 50000, 'Waiting for download success status.');
                        element(by.css('div[title="App status will be shown here"]')).isPresent().then(function(present) {
                            if (present) {
                                element(by.css('div[title="App status will be shown here"]')).getText().then(function(status) {
                                    if (status.toLowerCase() === 'fir coefficients downloaded to device successfully.') {
                                        defer.fulfill();
                                    } else {
                                        defer.reject('Error in importing coeff file.');
                                    }
                                })
                            }
                        })
                    })
                }
            });
        } else {
            defer.reject('coeffPath  : ' + coeffPath + ' does not exist.');
        }

        return defer.promise;
    },
    /**
     * Applies the testcase.
     * @param testCase Testcase object.
     * resolves when test case is applied.
     * rejects when coefficients are not imported.
     */
    "applyTestCase": function(testCase) {
        var defer = protractor.promise.defer();
        var self = this;
        if (Object.keys(testCase).indexOf('coeffPath') < 0) {
            defer.reject("coeffPath is not mentioned in the testCase. TestCase: " + testCase.id + " not applied.");
        } else {
            self.turnOnFir().then(function() {
                self.importCoeff(testCase.coeffPath).then(function() {
                    defer.fulfill();
                }, function(reason) {
                    defer.reject(reason);
                })
            });
        }
        return defer.promise;
    }
}
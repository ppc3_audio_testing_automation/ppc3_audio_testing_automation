/**
 * This file has the utils functions for the equalizer test component
 * applyTestCase() - performs the whole test of the equalizer component for the particular testCase
 *                            Operations  : 
 *                                   It will apply the gang input
 *                                   It will change the biquad name based on the configuration and gang input
 *                                   It will apply the input for both the channels
 *                                   It will record the audio and dump the audio file.
 * changeBiquadsName() - It will change the biquads name based on the configuration and the Gang Input
 * appendChangesToBiquadName() - It will add or remove the additional details to biquads name based on the type of changes
 * applyGangInput() -  It will reinitialize the availableBiquads value
 *                     It will apply the gang checkbox input based on the test cases
 * applyInputToBiquads() - It will set the biquad indices once
 *                         It will check if the biquad is available or not
 *                         It will switch on the biquad if it is available
 *                         It will apply the filterType and other key inputs if the biquad is available
 * isBiquadAvailable() - It will find whether the biquad is visible or not 
 * extractVisibleBiquads() - It will update the visible biquads array with the biquads indices which are all visible to the UI
 * startProcess() -  It will update the available biquad object by setting the appropriate biquad indices
 * applyFilterType() - It will apply the filter input if it is not undefined or null
 * applyKeyInputs() - It will apply the key inputs if that inputs are available for that particular filterType
 */
var visible_biquads = [];
var availableBiquads = { left: {}, right: {} };
var path = require('path');
module.exports = {
    "appendChangesToBiquadName": function(testCaseObject_biquad, nameToAppendorRemove, typeOfChanges) {
        Object.keys(testCaseObject_biquad).forEach(function(keys, index) {
            var new_biquad;
            if (typeOfChanges === 'apply')
                new_biquad = nameToAppendorRemove + keys;
            if (typeOfChanges === 'restore')
                new_biquad = keys.replace(nameToAppendorRemove, '');
            testCaseObject_biquad[new_biquad] = testCaseObject_biquad[keys];
            delete testCaseObject_biquad[keys];
        });
    },
    "applyFilterType": function(current_biquad, biquads, biquad_name, model) {
        var defer = protractor.promise.defer();
        if (biquads[biquad_name].filterType !== undefined || biquads[biquad_name].filterType !== '')
            current_biquad.all(by.model(model)).first().all(by.tagName('option')).reduce(function(result, option) {
                if (result)
                    return result;
                return option.getText().then(function(value) {
                    if (value.toLowerCase() === biquads[biquad_name].filterType.toLowerCase()) {
                        option.click();
                        return option;
                    }

                });
            }).then(function() {
                defer.fulfill();
            });
        return defer.promise;
    },
    "switchOnComponent": function() {
        var defer = protractor.promise.defer();
        element.all(by.css('.biquad-top-controls-holder')).first().all(by.css('.off-switch')).first().isDisplayed().then(function(display_of_offswitch) {
            if (display_of_offswitch) {
                element.all(by.css('.biquad-top-controls-holder')).first().all(by.css('.off-switch')).first().click().then(function() {

                });
            }
        }).then(function() {
            defer.fulfill();
        });
        return defer.promise;
    },
    "applyGangInput": function(gang) {
        var defer = protractor.promise.defer();
        availableBiquads.left = {};
        availableBiquads.right = {};
        element(by.id('channelSwitcher')).isDisplayed().then(function(displayed) {
            if (displayed) {
                element(by.id('gangedBiquad')).getAttribute('checked').then(function(value) {
                    if (value === "true" && gang === false || value === "false" && gang === true || value === null && gang === true) {
                        element.all(by.css('.checkboxcss3.check-ganged')).get(0).all(by.tagName("label")).first().click().then(function() {
                            browser.sleep(1000).then(function() {
                                defer.fulfill();
                            });
                        });
                    } else {
                        browser.sleep(1000).then(function() {
                            defer.fulfill();
                        });
                    }
                });
            } else {
                defer.fulfill();
            }
        });
        return defer.promise;
    },
    "applyKeyInputs": function(current_biquad, biquads, biquad_name) {
        var defer = protractor.promise.defer();
        var rejectTestCase = false;
        current_biquad.all(by.css('.cc-nd-input-title')).each(function(biquad_input, index_of_input) {
            biquad_input.getText().then(function(biquad_input_text) {
                var keyValue;
                switch (biquad_input_text) {
                    case "Gain":
                        keyValue = "gain";
                        break;
                    case "Frequency":
                        keyValue = "frequency";
                        break;
                    case "Q Value":
                        keyValue = "qVal";
                        break;
                    case "Bandwidth":
                        keyValue = "bandwidth";
                        break;
                    case "Ripple":
                        keyValue = "ripple";
                        break;
                    case "Lower Sensing Boundary":
                        keyValue = "lowerSensingBoundary";
                        break;
                    case "Upper Sensing Boundary":
                        keyValue = "upperSensingBoundary";
                        break;
                }
                if (Object.keys(biquads[biquad_name]).indexOf(keyValue) > -1 && biquads[biquad_name][keyValue] !== undefined) {
                    current_biquad.all(by.css('div[title="' + biquad_input_text + '"]')).first().click();
                    browser.wait(function() {
                        return current_biquad.all(by.css('.input-txt')).get(index_of_input).isDisplayed();
                    }, 50000, 'waiting for input field');
                    current_biquad.all(by.css('.input-txt')).get(index_of_input).clear();
                    current_biquad.all(by.css('.input-txt')).get(index_of_input).sendKeys(biquads[biquad_name][keyValue]);
                    biquad_input.click();
                }
            });
        }).then(function() {
            defer.fulfill();
        });
        return defer.promise;
    },
    "applyInputToBiquads": function(biquads, channel) {
        var count = 1;
        var defer = protractor.promise.defer();
        Object.keys(biquads).forEach(function(biquad_name) {
            module.exports.startProcess(count, biquads, channel).then(function() {
                count++;
                var current_biquad;
                module.exports.isBiquadAvailable(biquad_name, channel).then(function() {
                    current_biquad = element.all(by.css('.tabular_view_innersection.front')).get(availableBiquads[channel][biquad_name]);
                    module.exports.switchOnBiquad(current_biquad).then(function() {
                        module.exports.applyFilterType(current_biquad, biquads, biquad_name, 'biquad.props.filter').then(function() {
                            module.exports.applyKeyInputs(current_biquad, biquads, biquad_name).then(function() {
                                if (Object.keys(biquads).indexOf(biquad_name) === Object.keys(biquads).length - 1) {
                                    defer.fulfill();
                                }
                            }, function() {
                                defer.reject();
                            });
                        }, function() {
                            defer.reject();
                        });
                    });
                }, function() {

                });
            });
        });
        return defer.promise;
    },
    "changeBiquadsName": function(testCaseObject, config, typeofChanges) {
        if (config.indexOf('Mono') > -1) {
            if (testCaseObject.bqLeft !== undefined)
                module.exports.appendChangesToBiquadName(testCaseObject.bqLeft, 'EQ', typeofChanges);
        } else if (config.indexOf('Stereo') > -1) {
            if (testCaseObject.gang === true) {
                if (testCaseObject.bqLeft !== undefined)
                    module.exports.appendChangesToBiquadName(testCaseObject.bqLeft, 'L/R', typeofChanges);
            } else if (testCaseObject.gang === false) {
                if (testCaseObject.bqLeft !== undefined)
                    module.exports.appendChangesToBiquadName(testCaseObject.bqLeft, 'L', typeofChanges);
                if (testCaseObject.bqRight !== undefined)
                    module.exports.appendChangesToBiquadName(testCaseObject.bqRight, 'R', typeofChanges);
            }
        }
        return testCaseObject;
    },
    "extractVisibleBiquads": function() {
        var defer = protractor.promise.defer();
        visible_biquads = [];
        element.all(by.css('.tabular_view_innersection.front')).each(function(dom, index) {
            dom.isDisplayed().then(function(displayed) {
                if (displayed) {
                    visible_biquads.push(index);
                }
            });
        }).then(function() {
            defer.fulfill();
        });
        return defer.promise;
    },
    "isBiquadAvailable": function(biquad_name, channel) {
        var defer = protractor.promise.defer();
        if (availableBiquads[channel][biquad_name] !== undefined)
            element.all(by.css('.tabular_view_innersection.front')).get(availableBiquads[channel][biquad_name]).isDisplayed().then(function(displayed) {
                if (displayed) {
                    defer.fulfill();
                }
            });
        else
            defer.reject();
        return defer.promise;
    },
    "setBiquadIndices": function(biquads, channel) {
        var defer = protractor.promise.defer();
        visible_biquads.forEach(function(visible_bq, index) {
            element.all(by.css('.tabular_view_innersection.front')).get(visible_bq).all(by.css('.head-nos-holder')).get(0).getText().then(function(name) {
                if (Object.keys(biquads).indexOf(name) > -1) {
                    availableBiquads[channel][name] = visible_bq;
                }
            }).then(function() {
                if (index === visible_biquads.length - 1)
                    defer.fulfill();
            });
        });
        return defer.promise;
    },
    "switchOnBiquad": function(current_biquad) {
        var defer = protractor.promise.defer();
        current_biquad.all(by.css('.off-switch')).first().isDisplayed().then(function(displayed) {
            if (displayed) {
                current_biquad.all(by.css('.off-switch')).first().click();
            }
            defer.fulfill();
        })
        return defer.promise;
    },
    "startProcess": function(count, biquads, channel) {
        var defer = protractor.promise.defer();
        if (count === 1) {
            module.exports.setBiquadIndices(biquads, channel).then(function() {
                defer.fulfill();
            });
        } else {
            defer.fulfill();
        }
        return defer.promise;
    },
    "applyTestCase": function(common_utils, testCaseObject, audio_player, config, rootPath, runInput, device, defaultAudioInput, audioModeElement) {
        var defer = protractor.promise.defer();
        var audio;
        if (testCaseObject.audioFilePath !== undefined) {
            audio = testCaseObject.audioFilePath;
        } else {
            audio = defaultAudioInput;
        }
        if (config.indexOf('/') > -1) {
            var slashIndex = config.indexOf('/');
            config = config.substring(0, slashIndex) + config.substring(slashIndex + 1);
        }
        module.exports.applyGangInput(testCaseObject.gang).then(function() {
            module.exports.changeBiquadsName(testCaseObject, config, 'apply');
            if (testCaseObject.bqLeft !== undefined)
                module.exports.applyInputToBiquads(testCaseObject.bqLeft, 'left').then(function() {
                    if (config.indexOf('Mono') < 0 && testCaseObject.gang === false && testCaseObject.bqRight !== undefined) {
                        common_utils.switchChannel('right');
                        browser.sleep(2000);
                        module.exports.applyInputToBiquads(testCaseObject.bqRight, 'right').then(function() {
                            console.log('inputs applied!!!');
                            common_utils.recordAudio(rootPath, audio_player, testCaseObject, defaultAudioInput).then(function() {
                                common_utils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCaseObject.id, config, audioModeElement, device, runInput.appToTest);
                                common_utils.switchChannel('left');
                            });
                        }, function() {});
                    } else {
                        console.log('coming to else part');
                        common_utils.recordAudio(rootPath, audio_player, testCaseObject, defaultAudioInput);
                        common_utils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCaseObject.id, config, audioModeElement, device, runInput.appToTest);
                    }
                }).then(function() {
                    common_utils.applySnapshot().then(function() {
                        module.exports.changeBiquadsName(testCaseObject, config, 'restore');
                    });
                }, function() {
                    common_utils.applySnapshot().then(function() {
                        module.exports.changeBiquadsName(testCaseObject, config, 'restore');
                    });
                });
        }).then(function() {
            defer.fulfill();
        });
        return defer.promise;
    }
};
/**
 * This is a spec file to test the equalizer component
 * common_utils - has the common functionalities
 * testCasesJson - has the test cases for the equalizer component
 * eql_utils - has the common functionalities for equalizer component
 * audio_player - has the functionalities of audio player
 * The test() function has three test specs mentioned below
 *          test spec 1- (should switch on the device)
 *                      -> It will open the component
 *                      -> It will take screenShot
 *                      -> It will switch On the Component 
 *                      -> It will update the status of Device B availability
 *                      -> It will update the status of Visible biquads
 *          test spec 2- (should apply the test case)
 *                      -> It will apply the testcases for device A and device B (if it is available)
 *          test spec 3- (should close the device)
 *                      -> It will navigate back to the component page
 *                      -> It will turn off the component 
 */
var common_utils = require('../commonUtils.js');
var testCasesJson = require('./equalizer_testcases.json');
var eql_utils = require('./equalizer_Utils.js');
var audio_player = require('../audio_player/audio_player_operations.js');
module.exports = {
    test: function(rootPath, runInput, audio_player, config, audioModeElement) {
        var defer_for_equalizer = protractor.promise.defer();
        it('should switch on the equalizer component', function() {
            browser.ignoreSynchronization = true;
            common_utils.switchOnComponent('Equalizer').then(function(componentIndex) {
                common_utils.openComponent(componentIndex).then(function() {
                    eql_utils.extractVisibleBiquads().then(function(visible_bq) {});
                });
            });
        });
        var testCases = common_utils.findTestCasesToRun(testCasesJson.testCases, runInput, 'equalizer');
        testCases.forEach(function(testCaseObject) {
            it(testCaseObject.id, function() {
                browser.ignoreSynchronization = true;
                eql_utils.switchOnComponent().then(function() {
                    eql_utils.applyTestCase(common_utils, testCaseObject, audio_player, config, rootPath, runInput, 'A', testCasesJson.defaultAudioInput, audioModeElement).then(function() {
                        common_utils.isDeviceBAvailable().then(function() {
                            common_utils.switchDevice('B');
                            eql_utils.switchOnComponent().then(function() {
                                eql_utils.applyTestCase(common_utils, testCaseObject, audio_player, config, rootPath, runInput, 'B', testCasesJson.defaultAudioInput, audioModeElement).then(function() {
                                    common_utils.switchDevice('A');
                                });
                            });
                        }, function() {});
                    });
                });
            });
        }, this);
        it('close the device', function() {
            browser.ignoreSynchronization = true;
            browser.wait(function() {
                return element.all(by.css('.breadscrum-link')).get(2).isDisplayed();
            }, 50000, 'waiting for breadscrum link');
            element.all(by.css('.breadscrum-link')).get(2).click().then(function() {
                common_utils.switchOffComponent("Equalizer").then(function() {
                    defer_for_equalizer.fulfill();
                });
            });
        });
        return defer_for_equalizer.promise;
    }
};
var fs = require('fs');
var path = require('path');
var commonUtils = require('../commonUtils.js');
var chai = require('chai');
var expect = chai.expect;
var smartBass = require('../smartAmp/smartAmpUtils.js');
var thermalLimiter = require('./thermal_limiter_utils.js');

module.exports = {
    /**
     * Tests the Thermal Limiter.
     * @param rootPath Current working directory path.
     * @param runInput Contents of config.json file.
     * @param audioPlayer audio player component to call its functions.
     * @param currentConfig Name of the configurations which is currently opened.
     * @param currentAudioMode Name of the audio mode which is currently opened.
     */

    test: function(rootPath, runInput, audioPlayer, currentConfig, currentAudioMode) {
        var defer = protractor.promise.defer();
        var isComponentPresent = false;
        var isNotCombined = false,
            componentIndex, isSmartBass = false;
        it('should turn on Thermal Limiter', function() {
            commonUtils.switchOnComponent('Thermal Limiter').then(function(index) {
                commonUtils.isSpeakerNotCombined('Thermal Limiter').then(function() {
                    isNotCombined = true;
                }, function() {})
                componentIndex = index;
                isComponentPresent = true;
            }, function(reason) {
                commonUtils.switchOnComponent('Smart Bass').then(function(index) {
                    componentIndex = index;
                    isSmartBass = true;
                    commonUtils.isSpeakerNotCombined('Smart Bass').then(function() {
                        isNotCombined = true;
                    }, function() {}).then(function() {
                        isComponentPresent = true;
                        commonUtils.openComponent(componentIndex);
                    })
                }, function() {})
            }).then(function() {
                expect(isComponentPresent, 'Thermal limiter not present').to.be.true;
            })
        })
        var testInputs = require('./thermal_limiter_testcases.json');
        testCases = commonUtils.findTestCasesToRun(testInputs.testCases, runInput, "thermal_limiter");
        testCases.forEach(function(testCase, index) {
            it("should write test case " + testCase.id, function() {
                expect(isComponentPresent, 'Thermal limiter not present').to.be.true;
                if (isComponentPresent) {
                    if (isSmartBass) {
                        smartBass.turnOnSmartAmp();
                        thermalLimiter.applyTestCase(testCase, componentIndex, true);
                    } else {
                        thermalLimiter.turnOnThermalLimiter();
                        thermalLimiter.applyTestCase(testCase, componentIndex, false);
                    }
                    commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                        var deviceType;
                        if (isNotCombined) {
                            deviceType = 'Woofer_A';
                        } else {
                            deviceType = 'A';
                        }
                        commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, deviceType, runInput.appToTest);
                    });
                    commonUtils.applySnapshot();
                    commonUtils.isDeviceBAvailable().then(function() {
                        commonUtils.switchDevice('B');
                        if (isSmartBass) {
                            smartBass.turnOnSmartAmp();
                            thermalLimiter.applyTestCase(testCase, componentIndex, true);
                        } else {
                            thermalLimiter.turnOnThermalLimiter();
                            thermalLimiter.applyTestCase(testCase, componentIndex, false);
                        }
                        commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                            commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'B', runInput.appToTest);
                        });
                        commonUtils.applySnapshot();
                        commonUtils.switchDevice('A');
                    }, function(error) {}).then(function() {
                        if (isNotCombined) {
                            if (isSmartBass) {
                                commonUtils.goBackToAudioProcessingPage();
                            }
                            commonUtils.switchToSpeaker('Tweeter');
                            commonUtils.switchOnComponent('Thermal Limiter').then(function(componentIndex) {
                                thermalLimiter.applyTestCase(testCase, componentIndex, false);
                                commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                                    commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'Tweeter_A', runInput.appToTest);
                                });
                                commonUtils.applySnapshot();
                                commonUtils.switchToSpeaker('Woofer');
                            }, function() {
                                commonUtils.switchOnComponent("Smart Bass").then(function(index) {
                                    commonUtils.openComponent(index);
                                    thermalLimiter.applyTestCase(testCase, index, true);
                                    commonUtils.recordAudio(rootPath, audioPlayer, testCase, testInputs.defaultAudio).then(function() {
                                        commonUtils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, testCase.id, currentConfig, currentAudioMode, 'Tweeter_A', runInput.appToTest);
                                    });
                                    commonUtils.applySnapshot();
                                    commonUtils.goBackToAudioProcessingPage();
                                    commonUtils.switchToSpeaker('Woofer');
                                }, function() {})
                            }).then(function() {
                                if (isSmartBass) {
                                    commonUtils.openComponent(componentIndex);
                                }
                            })
                        }
                    }).then(function() {
                        if (index === testCases.length - 1) {
                            if (isSmartBass) {
                                commonUtils.goBackToAudioProcessingPage();
                            }
                            defer.fulfill();
                        }
                    })
                }
            })
        })
        return defer.promise;
    }
}
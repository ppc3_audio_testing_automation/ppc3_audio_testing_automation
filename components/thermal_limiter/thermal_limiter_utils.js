var commonUtils = require('../commonUtils.js');
var waitAbsent = require('../lib/waitAbsent.js');
module.exports = {
    /**
     * checks if the given key is present in the given object or not.
     * @param obj Object to find the keys in it.
     * @param key key to find its existence in the given object. 
     */
    "isKeyPresent": function(obj, key) {
        if (Object.keys(obj).indexOf(key) > -1) {
            return true;
        }
        return false;
    },
    /**
     * Sets the power limit of thermal limiter.
     * @param value the value to be set as power limit.
     * @param unit unit to be set for the power limit.
     * @param isSmartBass Boolean which is true when it is Smart Bass component.
     * @param componentIndex Index of the container which holds power limit. It is required if thermal limiter is itself present as a component in the UI.
     */
    "setPowerLimit": function(value, unit, isSmartBass, componentIndex) {
        var self = this;
        self.getComponentHolder(isSmartBass, componentIndex).then(function(componentHolder) {
            if (!isSmartBass) {
                componentHolder.all(by.css('.xmax-input')).first().clear();
                componentHolder.all(by.css('.xmax-input')).first().sendKeys(value);
            } else {
                componentHolder.all(by.css('.nominal-loudness-field-holder')).reduce(function(isValueSet, fieldHolder) {
                    if (isValueSet) {
                        return;
                    }
                    return fieldHolder.all(by.css('.samp-char-data-right-value')).first().isPresent().then(function(present) {
                        if (present) {
                            fieldHolder.all(by.css('.samp-char-data-right-value')).first().getText().then(function(heading) {
                                if (heading.toLowerCase() === 'power limit') {
                                    fieldHolder.all(by.css('.xmax-input')).first().clear();
                                    fieldHolder.all(by.css('.xmax-input')).first().sendKeys(value);
                                    return true;
                                }
                            })
                        }
                    })
                })
            }

        }).then(function() {
            element(by.model('charData.currentCharData.safeOperatingArea.limitUnits')).all(by.tagName('option')).reduce(function(isOptionFound, optionElm) {
                if (isOptionFound) {
                    return;
                }
                return optionElm.getText().then(function(option) {
                    if (option.toLowerCase() === unit.toLowerCase()) {
                        optionElm.click();
                        return true;
                    }
                })
            })
        })
    },
    /**
     * Gets the container which has the power limit controls. It is based on whether it is Thermal limiter or smart bass.
     * @param isSmartBass Boolean which is true when it is Smart Bass component.
     * @param componentIndex Index of the container which holds power limit. It is required if thermal limiter is itself present as a component in the UI.
     */
    "getComponentHolder": function(isSmartBass, componentIndex) {
        var defer = protractor.promise.defer();
        var componentHolder;
        if (!isSmartBass) {
            componentHolder = element.all(by.css('.grid_innerdiv')).get(componentIndex);
            defer.fulfill(componentHolder);
        } else {
            element.all(by.css('.tflow-flip-frontpanel')).reduce(function(isPowerLimitPanelFound, panelHolder) {
                if (isPowerLimitPanelFound) {
                    return;
                }
                return panelHolder.all(by.css('.tw-panels-heading')).first().getText().then(function(panelName) {
                    if (panelName.toLowerCase() === 'max level tuning') {
                        componentHolder = panelHolder;
                    }
                })
            }).then(function() {
                defer.fulfill(componentHolder);
            })
        }
        return defer.promise;
    },
    /**
     * Sets the time constants.
     * @param timeConstants An object which has the property and values for timeconstants.
     * @param isSmartBass Boolean which is true when it is Smart Bass component.
     * @param componentIndex Index of the container which holds power limit. It is required if thermal limiter is itself present as a component in the UI.
     */
    "setTimeConstants": function(timeConstants, isSmartBass, componentIndex) {
        var defer = protractor.promise.defer();
        var self = this;
        self.getComponentHolder(isSmartBass, componentIndex).then(function(componentHolder) {
            componentHolder.all(by.css('.sa-max-level-settings-holder')).first().isPresent().then(function(present) {
                if (present) {
                    componentHolder.all(by.css('.sa-max-level-settings-holder')).first().click();
                    browser.wait(function() {
                        return element(by.css('div[ng-show="saMaxLevelPopup"]')).all(by.css('.custom-popup-content-holder')).first().isDisplayed();
                    }, 20000, 'Waiting for timeconstant popup to appear.');
                    if (isSmartBass) {
                        element(by.css('div[ng-show="saMaxLevelPopup"]')).all(by.css('.custom-popup-content-holder')).first().all(by.css('.sa-max-level-pup-container')).reduce(function(isValueSet, pupHolder) {
                            if (isValueSet) {
                                return;
                            }
                            return pupHolder.all(by.css('.field-heading')).first().getText().then(function(fieldHeading) {
                                if (fieldHeading.toLowerCase() === 'thermal time constants') {
                                    Object.keys(timeConstants).forEach(function(timeconstant) {
                                        return pupHolder.all(by.css('.spatializer-inner-controls-col')).reduce(function(isConstantSet, controlHolder) {
                                            if (isConstantSet) {
                                                return true;
                                            }
                                            return controlHolder.all(by.css('.field-heading')).first().getText().then(function(constant) {
                                                if (constant.toLowerCase() === timeconstant.toLowerCase()) {
                                                    controlHolder.all(by.css('.input-txt')).first().clear();
                                                    controlHolder.all(by.css('.input-txt')).first().sendKeys(timeConstants[timeconstant]);
                                                    return true;
                                                }
                                            })
                                        })
                                    })
                                }
                            })
                        }).then(function() {
                            defer.fulfill();
                        })
                    } else {
                        Object.keys(timeConstants).forEach(function(timeconstant, index) {
                            element(by.css('div[ng-show="saMaxLevelPopup"]')).all(by.css('.custom-popup-content-holder')).first().all(by.css('.spatializer-inner-controls-col')).reduce(function(isConstantSet, controlHolder) {
                                if (isConstantSet) {
                                    return;
                                }
                                return controlHolder.all(by.css('.field-heading')).first().getText().then(function(constant) {
                                    if (constant.toLowerCase() === timeconstant.toLowerCase()) {
                                        controlHolder.all(by.css('.input-txt')).first().clear();
                                        controlHolder.all(by.css('.input-txt')).first().sendKeys(timeConstants[timeconstant]);
                                    }
                                })
                            }).then(function() {
                                if (index === Object.keys(timeConstants).length - 1) {
                                    defer.fulfill();
                                }
                            })
                        })
                    }
                }
            });
        })
        return defer.promise;
    },
    /**
     * Turns on the thermal limiter component.
     */
    "turnOnThermalLimiter": function() {
        element(by.css('div[title="Turn On Thermal Limiter/AGL"]')).isDisplayed().then(function(displayed) {
            if (displayed) {
                element(by.css('div[title="Turn On Thermal Limiter/AGL"]')).click();
            }
        })
    },
    /**
     * Applies the testcase to thermal limiter.
     * @param testCase testcase object.
     * @param isSmartBass Boolean which is true when it is Smart Bass component.
     * @param componentIndex Index of the container which holds power limit. It is required if thermal limiter is itself present as a component in the UI.
     */
    "applyTestCase": function(testCase, componentIndex, isSmartBass) {
        var self = this;
        if (isSmartBass) {
            browser.wait(function() {
                return element.all(by.css('.tflow-top-right-container')).first().isDisplayed();
            }, 50000, 'Waiting for the smart bass page');
            element(by.css('label[for="powerLimit"]')).click();
        }
        if (self.isKeyPresent(testCase, 'powerLimit')) {
            if (!self.isKeyPresent(testCase, 'powerLimitUnit')) {
                testCase.powerLimitUnit = "Watt Cont.";
            }
            self.setPowerLimit(testCase.powerLimit, testCase.powerLimitUnit, isSmartBass, componentIndex);
        }
        if (self.isKeyPresent(testCase, 'timeConstants')) {
            self.setTimeConstants(testCase.timeConstants, isSmartBass, componentIndex).then(function() {
                element(by.css('div[ng-click="hideSaMaxLevelPopup()"]')).click();
                browser.wait(function() {
                    return element.all(by.css('div[ng-show="saMaxLevelPopup"]')).first().waitAbsent();
                }, 20000, 'Waiting for time constant popup to close');
            })
        }
    }
}
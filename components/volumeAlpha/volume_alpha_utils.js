module.exports = {
    /**
     * sets the volume alpha.
     * @param value sets the value to volume alpha.
     * resolves when value is set to volume alpha.
     * rejects when the test case does not have volumeAlpha field.
     */
    "setVolumeAlpha": function(value) {
        var defer = protractor.promise.defer();
        if (value == undefined) {
            defer.reject('volumeAlpha is not specified in the test case.');
        } else {
            browser.wait(function() {
                return element.all(by.css(".digi-gain-ddown-speaker")).first().isDisplayed();
            }, 10000, "waiting for volume icon");
            element.all(by.css(".digi-gain-ddown-speaker")).first().click();
            browser.wait(function() {
                return element.all(by.css(".digigain-vertical-slider-value")).first().isDisplayed();
            }, 10000, "waiting for volume slider field");
            browser.sleep(150);
            element(by.css('.va-controls-holder')).isPresent().then(function(present) {
                if (present) {
                    element(by.css('.va-controls-holder')).isDisplayed().then(function(displayed) {
                        if (displayed) {
                            element(by.model('appData.volumeAlpha.props.volAlpha.value')).clear();
                            element(by.model('appData.volumeAlpha.props.volAlpha.value')).sendKeys(value);
                        } else {
                            element(by.css('.va-down-arr-holder')).click().then(function() {
                                browser.wait(function() {
                                    return element(by.css('.va-controls-holder')).isDisplayed();
                                }, 2000, 'Waiting for volume alpha controls holder.');
                                element(by.model('appData.volumeAlpha.props.volAlpha.value')).clear();
                                element(by.model('appData.volumeAlpha.props.volAlpha.value')).sendKeys(value);
                            });
                        }
                    })
                }
            }).then(function() {
                element.all(by.css(".digi-gain-ddown-speaker")).first().click();
                defer.fulfill();
            })
        }
        return defer.promise;
    }
}
var configuration_file_input = require('./config.json');
exports.config = {};
exports.config.framework = 'mocha';
exports.config.directConnect = true;
exports.config.specs = ['spec.js'];
exports.config.chromeDriver = configuration_file_input.chromeDriverPath;
exports.config.params = {
    console: [],
    time: []
};
exports.config.mochaOpts = {
    ui: 'bdd',
    reporter: "spec",
    timeout: 60000
};

exports.config.onComplete = function() {
    var fs = require('fs');
    if (!fs.existsSync('output_files')) {
        fs.mkdirSync('output_files');
    }
};
exports.config.resultJsonOutputFile = './output_files/report.json';
exports.config.afterLaunch = function() {
    excel_generator.generateExcel(configuration_file_input.appToTest);
};
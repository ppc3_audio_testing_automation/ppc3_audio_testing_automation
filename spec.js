var fs = require('fs');
var path = require('path');
var components = require(__dirname + '/components/components.js');
var common_utils = require('./components/commonUtils.js');


function getConfigurations(config, runInput) {
    // If appname is not specified, it will create the key as an appname
    if (runInput[runInput.appToTest] === undefined) {
        runInput[runInput.appToTest] = {};
    }
    // If configs are not present it will get the configurations present in the app from the app's config.json file
    if (runInput[runInput.appToTest].configs === undefined || runInput[runInput.appToTest].configs.length === 0) {
        var configurations = [];
        Object.keys(config.configTypes).forEach(function(conf, position) {
            configurations.push(config.configTypes["" + conf + ""].name);
        });
        return configurations;
    } else {
        // If configs are mentioned in the config.json, it will return that array.
        return runInput[runInput.appToTest].configs;
    }
}

function getAudioModes(config, runInput, configuration) {
    var audioModes = {};
    // If audioModes are not mentioned in config.json it will create an key for that
    if (runInput[runInput.appToTest].audioModes === undefined) {
        runInput[runInput.appToTest].audioModes = {};
    }
    // This will update the audio mode details if not mentioned in config.json
    configuration.forEach(function(confEle, position) {
        if (runInput[runInput.appToTest].audioModes === undefined || runInput[runInput.appToTest].audioModes['' + confEle + ''] === undefined || runInput[runInput.appToTest].audioModes['' + confEle + ''].length === 0) {
            Object.keys(config.configTypes).filter(function(conf) {
                if (config.configTypes[conf].name === confEle)
                    configObject = config.configTypes[conf];
            });
            var hybridFlowDetails = require(path.dirname(runInput.chromeDriverPath) + '/node_modules/ti_addonmgr/node_modules/TAS5825M/app/plugins/TAS5825M' + configObject.configFolderPath + '/hybrid_flow_details.json');
            var audioModesArray = [];
            Object.keys(hybridFlowDetails).forEach(function(audioMode) {
                audioModesArray.push(hybridFlowDetails[audioMode].title.flow + ' ' + hybridFlowDetails[audioMode].title.mode);
            });
            runInput[runInput.appToTest].audioModes[confEle] = audioModesArray;
        }
    });
    return runInput;
}

if (fs.existsSync('./config.json')) {
    describe('Audio Testing Automation', function() {
        after(function() {
            components.excel_generator.checkConsole();
        });
        var runInput = require('./config.json');
        var config = require(path.dirname(runInput.chromeDriverPath) + '/node_modules/ti_addonmgr/node_modules/TAS5825M/app/plugins/TAS5825M/config/components/config.json');
        var rootPath = __dirname;
        var dragAndDropOnce = true;
        var commonAudioUploadOnce = true;
        var configurations = getConfigurations(config, runInput);
        runInput = getAudioModes(config, runInput, configurations);
        it('should resolve the uncaughtexception in ' + runInput.appToTest, function() {
            components.navigate.resolvePopup(rootPath);
        });
        configurations.forEach(function(config, configPosition) {
            var audioModes = runInput[runInput.appToTest].audioModes["" + config + ""];
            it('should navigate to processflow page of ' + config, function() {
                browser.ignoreSynchronization = true;
                components.navigate.navigateUpToTunningPage(config, configPosition, runInput);
            });
            audioModes.forEach(function(audioModeElement, audioModePosition) {
                it('should navigate to ' + audioModeElement + ' and save the ppc3 file', function() {
                    browser.ignoreSynchronization = true;
                    components.navigate.navigateThroughAudioModes(config, audioModeElement).then(function() {
                        if (dragAndDropOnce === true) {
                            components.audio_player.openAudioPlayer().then(function() {
                                components.audio_player.makeRecordButtonVisible();
                                dragAndDropOnce = false;
                                if (commonAudioUploadOnce === true) {
                                    components.audio_player.uploadAudio(runInput.commonAudioInput);
                                    components.audio_player.recordAudio().then(function() {
                                        common_utils.dumpAudioFile(path.dirname(runInput.chromeDriverPath), rootPath, 'ref', 'allConfigs', 'all', 'A&B', runInput.appToTest);
                                    });
                                    commonAudioUploadOnce = false;
                                }
                                components.audio_player.closeAudioPlayer();
                            })
                        }
                    }).then(function() {
                        common_utils.takeSnapshot();
                    });
                });
                runInput.componentsToTest.forEach(function(component) {
                    components[component].test(rootPath, runInput, components.audio_player, config, audioModeElement).then(function() {
                        common_utils.resetTuning();
                    });
                });
                it('should change the audio mode', function() {
                    browser.ignoreSynchronization = true;
                    components.navigate.changeAudioMode();
                });
            });
            it('should navigate to home page', function() {
                browser.ignoreSynchronization = true;
                components.navigate.navigateBackToHomePage();
                dragAndDropOnce = true;
            });
        });
    });
}